package cz.ajio.simpleopencv1;

import java.io.File;
import java.io.FileWriter;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.ListIterator;
import java.util.Locale;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.CameraBridgeViewBase.CvCameraViewFrame;
import org.opencv.android.CameraBridgeViewBase.CvCameraViewListener2;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfKeyPoint;
import org.opencv.core.Scalar;
import org.opencv.features2d.FeatureDetector;
import org.opencv.features2d.Features2d;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.hardware.Camera.Size;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.SubMenu;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.WindowManager;
import android.widget.Toast;

public class HelloOpenCvActivity extends Activity implements CvCameraViewListener2, OnTouchListener 
		// ONTOUCH - pro metodu onTouch ktera uklada screenshoty
		// rozliseni se musi nastavit v instanci kamery v java
{
	public static final String TAG = "Ajio_DPproject";
	public static final String myFileDir = "DPproject";
	private HelloOpenCvView mOpenCvCameraView;
	private Mat mRgba, mGray, mIntermediateMat, mMat, mMat_keys;
	private Mat mRot, mTrans, mRvec, mQuat, mTrans2, mQuat2;

	// tyto oCV obejkty se zde mohou jen takto napsat
	// samotna inicializace se MUSI delat az po nacteni oCV
	private FeatureDetector mFeatureDetector;
	private MatOfKeyPoint mMatOfKeyPoint;

	private List<Size> mResolutionList;
	private MenuItem[] mResolutionMenuItems;
	private SubMenu mResolutionMenu;

	private SensorManager mSensorManager;
	private Sensor mSensorLinearAcceleration, mSensorGyroscope, mSensorRotationVector;

	private static final int VIEW_MODE_RGBA = 0;
	private static final int VIEW_MODE_GRAY = 1;
	private static final int VIEW_MODE_AUGMENT = 5;
	private static final int VIEW_MODE_MSER = 3;

	private static final int FILE_TXT_CAM = 1;
	private static final int FILE_TXT_SEN = 2;
	private static final int FILE_IMAGE = 3;
	// f in Hz * time in sec * sensor count = total time
	private static final int BUFFER_SIZE = 200 * 60 * 3; // jinymi slovy buffer je alokovan na minutu behu senzoru
	private static final int TYPE_GYRO_INT = 99; // konstanta pro rozliseni integrace gyra
	private static String fileTimeStamp;
	private static int fileIteration;

	private int mViewMode;
	private boolean patternFound, bufferOpened = false;
	private boolean touched = false;
	private double[] bufferRot = new double[9];
	private double[] bufferTrans = new double[3];
	private double[] bufferRvec = new double[3];
	private double[] bufferQuat = new double[4];
	private double[] bufferTrans2 = new double[3];
	private double[] bufferQuat2 = new double[4];
	private long timePattern, timeGyro, timeLinAcc, timeRotVec, timeFrame;
	private long startTime1, elapsedTime1, startTime2, elapsedTime2;
	private int counterCam = 0, counterSen = 0;
	// druhy rozmer kamery = time stamp, rotation, translation, rotvec, quat
	// druhy rozmer senzoru = time samp, data (xyz/wxyz(quat)), sensor type
	private double[][] bufferCameraPosition = new double[BUFFER_SIZE][1 + 9 + 3 + 3 + 4 + 3 + 4];
	private double[][] bufferSensorData = new double[BUFFER_SIZE][1 + 4 + 1];

	private MenuItem mItemPreviewRGBA, mItemPreviewGray, mItemPreviewFeatures, mPreviousItem;
	private MenuItem mItemPreviewMSER, mItemGoToSensorTest;
	private static final Scalar KEY_COLOR = new Scalar(0, 255, 0, 255);
	
	Handler mHandler = new Handler(); // for thread experiments
	private SensorBuffer mSensorBuffer;
	
	private final SensorEventListener mSensorListener = new SensorEventListener() {
		
		private final float[] oriQuaternion = new float[4]; // pro ulozeni orientace ze senzoru rotation vector
		private static final float NS2S = 1.0f / 1000000000.0f;
		private static final float EPSILON = 1.0f / 1000000000.0f;
		private final float[] deltaQuaternion = new float[4];
		private long timestampGyro;
		
		@Override
		public void onSensorChanged(SensorEvent event) {
			switch (event.sensor.getType()) {
			case Sensor.TYPE_GYROSCOPE:
				timeGyro = System.nanoTime();
				  // This timestep's delta rotation to be multiplied by the current rotation
				  // after computing it from the gyro sample data.
				  if (timestampGyro != 0) {
				    final float dT = (event.timestamp - timestampGyro) * NS2S;
				    // Axis of the rotation sample, not normalized yet.
				    float axisX = event.values[0];
				    float axisY = event.values[1];
				    float axisZ = event.values[2];
				    // Calculate the angular speed of the sample
				    float omegaMagnitude = (float) Math.sqrt((double) (axisX*axisX + axisY*axisY + axisZ*axisZ));
				    // Normalize the rotation vector if it's big enough to get the axis
				    // (that is, EPSILON should represent your maximum allowable margin of error)
				    if (omegaMagnitude > EPSILON) {
				      axisX /= omegaMagnitude;
				      axisY /= omegaMagnitude;
				      axisZ /= omegaMagnitude;
				    }
				    // Integrate around this axis with the angular speed by the timestep
				    // in order to get a delta rotation from this sample over the timestep
				    // We will convert this axis-angle representation of the delta rotation
				    // into a quaternion before turning it into the rotation matrix.
				    float thetaOverTwo = (omegaMagnitude * dT) / 2.0f;
				    float sinThetaOverTwo = (float) Math.sin(thetaOverTwo);
				    float cosThetaOverTwo = (float) Math.cos(thetaOverTwo);
				    deltaQuaternion[0] = cosThetaOverTwo;
				    deltaQuaternion[1] = sinThetaOverTwo * axisX;
				    deltaQuaternion[2] = sinThetaOverTwo * axisY;
				    deltaQuaternion[3] = sinThetaOverTwo * axisZ;
				  }
				  timestampGyro = event.timestamp;
				    // User code should concatenate the delta rotation we computed with the current rotation
				    // in order to get the updated rotation.
				    // rotationCurrent = rotationCurrent * deltaRotationMatrix;

				addDataToBuffer(timeGyro, event.values, event.sensor.getType());
				addDataToBuffer(timeGyro, deltaQuaternion, TYPE_GYRO_INT); // ONLY SEGMENT INTEGRATION, NOT TOTAL !!!
				
				
				//Log.i(TAG, String.format("Sen Gyro %.2f %.2f %.2f ,%d",
				//		event.values[0], event.values[1], event.values[2],
				//		event.values.length));

				break;
			case Sensor.TYPE_LINEAR_ACCELERATION:
				timeLinAcc = System.nanoTime();
				
				addDataToBuffer(timeLinAcc, event.values,
				event.sensor.getType());
				
				//Log.i(TAG, String.format("Sen LinAcc %.2f %.2f %.2f ,%d",
				//		event.values[0], event.values[1], event.values[2],
				//		event.values.length));
				/*
				 * if (counter == 0 || counter == measuresPerInterval) { // �as
				 * v sec pro 1000 vzork� //String s =
				 * String.valueOf("linacc time: "
				 * +(event.timestamp-lastEventTime)/1000000000.0); // �as f v Hz
				 * z 1000 vzork� String s = String.valueOf("linacc freq: "+
				 * ((1000000000.0
				 * *measuresPerInterval)/(event.timestamp-lastEventTime)) +
				 * " Hz"); lastEventTime = event.timestamp; Log.i(TAG, s);
				 * counter = 0; } counter++;
				 * 
				 */
				break;
			case Sensor.TYPE_GAME_ROTATION_VECTOR: // TYPE_ROTATION_VECTOR
				timeRotVec = System.nanoTime();
				// normalized [w, x, y, z]
				SensorManager.getQuaternionFromVector(oriQuaternion, event.values); // w x y z representation
				mSensorBuffer.setOriQuaternion(oriQuaternion, timeRotVec);
				addDataToBuffer(timeRotVec, oriQuaternion, event.sensor.getType());
				Log.i(TAG, String.format("event mSensQuat %.2f %.2f %.2f , values %d, type %d",
						event.values[0], event.values[1], event.values[2],
						event.values.length, event.sensor.getType()));
				Log.i(TAG, String.format("source quat mSensQuat %.2f %.2f %.2f %.2f , values %d, type %d",
						oriQuaternion[0], oriQuaternion[1], oriQuaternion[2], oriQuaternion[3],
						event.values.length, event.sensor.getType()));
				break;
			default:
				// tento toast to hazi porad
				break;
			}
		}

		@Override
		public void onAccuracyChanged(Sensor sensor, int accuracy) {
			// TODO Auto-generated method stub
		}
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		Log.i(TAG, "called onCreate");
		super.onCreate(savedInstanceState);
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		setContentView(R.layout.helloopencvlayout);
		mOpenCvCameraView = (HelloOpenCvView) findViewById(R.id.HelloOpenCvView2);
		
		// all of this will be overwritten if JavaCameraView method initializeCamera is modified - hardcoded
		mOpenCvCameraView.setMinimumWidth(640);
		mOpenCvCameraView.setMinimumHeight(480);
		mOpenCvCameraView.setMaxFrameSize(640, 480); // max size of an initial preview resolution
		mOpenCvCameraView.setScaleX(1);
		mOpenCvCameraView.setScaleY(1);

		mOpenCvCameraView.setVisibility(SurfaceView.VISIBLE);
		mOpenCvCameraView.setCvCameraViewListener(this);
		mOpenCvCameraView.enableFpsMeter();

		mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
		mSensorLinearAcceleration = mSensorManager
				.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION);
		mSensorGyroscope = mSensorManager
				.getDefaultSensor(Sensor.TYPE_GYROSCOPE);
		mSensorRotationVector = mSensorManager
				.getDefaultSensor(Sensor.TYPE_GAME_ROTATION_VECTOR);
		
		isExternalStorageWritable(); // mo�n� nen� pot�eba
		
		for (int i = 0; i < BUFFER_SIZE; i++) { // double[] row: bufferSensorData
			Arrays.fill(bufferSensorData[i], Double.MAX_VALUE);
			Arrays.fill(bufferCameraPosition[i], Double.MAX_VALUE);
		} 
		
		fileTimeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.ENGLISH)
		.format(new Date()); // default value for screenshots
		fileIteration = 0;

	}

	private BaseLoaderCallback mLoaderCallback = new BaseLoaderCallback(this) {
		@Override
		public void onManagerConnected(int status) {
			switch (status) {
			case LoaderCallbackInterface.SUCCESS: {
				Log.i(TAG, "OpenCV loaded successfully");
				System.loadLibrary("L_detect");
				mOpenCvCameraView.enableView();
				mOpenCvCameraView.setOnTouchListener(HelloOpenCvActivity.this);
				// az zde vime ze se oCV nacetlo
				
				mFeatureDetector = FeatureDetector.create(FeatureDetector.MSER);
				mMatOfKeyPoint = new MatOfKeyPoint();
				mRot = new Mat(); mRot.create(4,1,CvType.CV_64F); // size will be modified automaticaly
				mTrans = new Mat(); mTrans.create(4,1,CvType.CV_64F);
				mRvec = new Mat(); mRvec.create(4,1,CvType.CV_64F);
				mQuat = new Mat(); mQuat.create(4,1,CvType.CV_64F);
				mTrans2 = new Mat(); mTrans2.create(4,1,CvType.CV_64F);
				mQuat2 = new Mat(); mQuat2.create(4,1,CvType.CV_64F);
				mSensorBuffer = new SensorBuffer();
				patternFound = false;
				
			}
				break;
			default: {
				super.onManagerConnected(status);
			}
				break;
			}
		}
	};
	

	/** ----- RESUME PAUSE DESTROY ----- */
	@Override
	public void onResume() {
		super.onResume();
		OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_3_0_0, this,
				mLoaderCallback); // you need to set it here also !!! and in Android.mk
	}

	@Override
	public void onPause() {
		super.onPause();
		if (mOpenCvCameraView != null)
			mOpenCvCameraView.disableView();
		mSensorManager.unregisterListener(mSensorListener);
	}

	public void onDestroy() {
		super.onDestroy();
		if (mOpenCvCameraView != null)
			mOpenCvCameraView.disableView();
	}

	/** ----- MENU ----- */
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		Log.i(TAG, "called onCreateOptionsMenu");
		// Inflate the menu; this adds items to the action bar if it is present.
		// getMenuInflater().inflate(R.menu.hello_open_cv, menu);

		int idx = 0;
		mResolutionMenu = menu.addSubMenu("Resolution");
		mResolutionList = mOpenCvCameraView.getResolutionList();
		mResolutionMenuItems = new MenuItem[mResolutionList.size()];

		Collections.sort(mResolutionList, new Comparator<Size>() {
			@Override
			public int compare(Size s1, Size s2) {
				return s1.width - s2.width;
			}
		});
		ListIterator<Size> resolutionItr = mResolutionList.listIterator();
		while (resolutionItr.hasNext()) {
			Size element = resolutionItr.next();
			mResolutionMenuItems[idx] = mResolutionMenu.add(2, idx, Menu.NONE,
					Integer.valueOf(element.width).toString() + "xxx"
							+ Integer.valueOf(element.height).toString());
			idx++;
		}

		mItemPreviewRGBA = menu.add("Preview RGBA");
		// mItemPreviewGray = menu.add("Preview GRAY");
		mItemPreviewFeatures = menu.add("Find L-pattern");
		mItemPreviewMSER = menu.add("Find MSER");
		mItemGoToSensorTest = menu.add("Sensor Test");

		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		Log.i(TAG, "called onOptionsItemSelected; selected item: " + item);
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		/*
		 * int id = item.getItemId(); if (id == R.id.action_settings) { return
		 * true; }
		 */
		if (item != mItemPreviewFeatures) {
			mSensorManager.unregisterListener(mSensorListener);
			if (counterCam > 0 || counterSen > 0) {
				
				final double[][] buffCameraPosition = bufferCameraPosition.clone();
				final double[][] buffSensorData = bufferSensorData.clone();
				
				Thread t = new Thread(new Runnable(){
		            @Override
		            public void run() {
		            	
			            	mHandler.post(new Runnable() {
		                        @Override
		                        public void run() {
		                        	Toast.makeText(getBaseContext(), "Writing data...", Toast.LENGTH_SHORT).show();
		                        }
		                    });
			            	writeData(buffCameraPosition);
							writeData(buffSensorData);
							for (int i = 0; i < BUFFER_SIZE; i++) { // double[] row: bufferSensorData
								Arrays.fill(bufferSensorData[i], Double.MAX_VALUE);
								Arrays.fill(bufferCameraPosition[i], Double.MAX_VALUE);
							} 
							counterCam = 0;
							counterSen = 0;
							
		                    mHandler.post(new Runnable() {
		                        @Override
		                        public void run() {
		                        	Toast.makeText(getBaseContext(), "Writing data = COMPLETED.", Toast.LENGTH_SHORT).show();
		                        }
		                    });
		                }
		            });
		            t.start();
				
				// OLD version in single thread
				/*Toast.makeText(this, "Writing data, preview is paused.", Toast.LENGTH_SHORT).show();
				writeData(bufferCameraPosition);
				writeData(bufferSensorData);
				for (int i = 0; i < BUFFER_SIZE; i++) { // double[] row: bufferSensorData
					Arrays.fill(bufferSensorData[i], Double.MAX_VALUE);
					Arrays.fill(bufferCameraPosition[i], Double.MAX_VALUE);
				} 
				counterCam = 0;
				counterSen = 0;
				Toast.makeText(this, "Writing data, completed.", Toast.LENGTH_SHORT).show();*/
			}
			
		}
		if (item.getGroupId() == 2) { // neb je tam natvrdo nadefinovan�
										// settings asi
			int id = item.getItemId();
			Size resolution = mResolutionList.get(id);
			mOpenCvCameraView.setResolution(resolution);
			Toast.makeText(this, Integer.valueOf(resolution.width).toString() + "-xyz1-"
					+ Integer.valueOf(resolution.height).toString(), Toast.LENGTH_SHORT).show();
			resolution = mOpenCvCameraView.getResolution();
			String caption = Integer.valueOf(resolution.width).toString() + "-xyz2-"
					+ Integer.valueOf(resolution.height).toString();
			Toast.makeText(this, caption, Toast.LENGTH_SHORT).show();
		} else if (item == mItemPreviewRGBA) {
			mViewMode = VIEW_MODE_RGBA;
		} else if (item == mItemPreviewGray) {
			mViewMode = VIEW_MODE_GRAY;
		} else if (item == mItemPreviewFeatures) {
			
			if (mPreviousItem != mItemPreviewFeatures) {
				fileTimeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.ENGLISH)
				.format(new Date());
				// pouzije nove soubory pro zapis dat a zapne senzory
	
				if (!mSensorManager.registerListener(mSensorListener,
						 mSensorRotationVector, SensorManager.SENSOR_DELAY_FASTEST)) {
						 	Toast.makeText(this, "rotvec error, or already registered", Toast.LENGTH_SHORT).show(); 
				 } 
				
				 if (!mSensorManager.registerListener(mSensorListener,
					 mSensorLinearAcceleration, SensorManager.SENSOR_DELAY_FASTEST)) {
					 	Toast.makeText(this, "linacc error, or already registered", Toast.LENGTH_SHORT).show();
				 } 
				 
				 if (!mSensorManager.registerListener(mSensorListener,
					 mSensorGyroscope, SensorManager.SENSOR_DELAY_FASTEST)) {
					 	Toast.makeText(this, "gyro error, or already registered", Toast.LENGTH_SHORT).show(); 
				 }
				 Toast.makeText(this, "Recording started.", Toast.LENGTH_SHORT).show(); 
			} else {
				Toast.makeText(this, "First, turn off recording, by switching to another mode.", Toast.LENGTH_SHORT).show(); 
			}
			 
			 
			mViewMode = VIEW_MODE_AUGMENT;
		} else if (item == mItemPreviewMSER) {
			mViewMode = VIEW_MODE_MSER;
		} else if (item == mItemGoToSensorTest) {
			Intent intent = new Intent(this, SensorTestActivity.class);
			startActivity(intent);
		}
		
		mPreviousItem = item;
		return super.onOptionsItemSelected(item);
	}

	/** ----- CAMERA WORK ----- */
	@Override
	public void onCameraViewStarted(int width, int height) {
		// TODO Auto-generated method stub
		/*
		 * // nastavovalo se tu akorat full hd dle prvnicho spusteneho rozliseni
		 * mMat = new Mat(height, width, CvType.CV_8UC4); mMat_keys = new
		 * Mat(height, width, CvType.CV_8UC4);
		 */
		Toast.makeText(this, "Camera View Started, res: " + width + "x" + height, Toast.LENGTH_SHORT).show();
	}

	@Override
	public void onCameraViewStopped() {
		// TODO Auto-generated method stub

	}

	@Override
	public Mat onCameraFrame(CvCameraViewFrame inputFrame) {
		final long frameTime = System.nanoTime();
		final Mat oriQuatMat = mSensorBuffer.getOriQuaternionMat(frameTime);
		Log.i(TAG, "java frametime " + frameTime);
		final int viewMode = mViewMode;
		switch (viewMode) {
		case VIEW_MODE_RGBA:
			// input frame has RBGA format
			mRgba = inputFrame.rgba();
			break;
		case VIEW_MODE_GRAY:
			// input frame has gray scale format
			Imgproc.cvtColor(inputFrame.gray(), mRgba, Imgproc.COLOR_GRAY2RGBA,
					4);
			break;
		case VIEW_MODE_AUGMENT:
			// input frame has RGBA format
			mRgba = inputFrame.rgba();
			mGray = inputFrame.gray();

			//android.os.Debug.waitForDebugger(); // for cpp debug - but app stops when no debug attached
			startTime1 = System.nanoTime();
			patternFound = FindFeatures(mGray.getNativeObjAddr(),
					mRgba.getNativeObjAddr(), mRot.getNativeObjAddr(),
					mTrans.getNativeObjAddr(), mRvec.getNativeObjAddr(),
					mQuat.getNativeObjAddr(), oriQuatMat.getNativeObjAddr(),
					mTrans2.getNativeObjAddr(), mQuat2.getNativeObjAddr());
			timePattern = System.nanoTime();
			elapsedTime1 = timePattern - startTime1;
			Log.i(TAG, "L-pattern : " + elapsedTime1 / 1000000 + " mils "
					+ ", size: " + mRgba.width() + " x " + mRgba.height());
			// elapsedTime1 je �as vyhodnocen� L patternu p�i nalezen� i
			// nenalezen� cca 30-60 mils
			// patternTime je proroti tomu �asov� zn�mka nalezen�ch L pattern�
			// a proto m� mnohem del�� intervaly cca
			Log.i(TAG,
					patternFound + " * " + mRot.toString() + " XXX "
							+ mTrans.toString());
			
			mRot.get(0, 0, bufferRot);
			mTrans.get(0, 0, bufferTrans);
			mRvec.get(0, 0, bufferRvec);
			mQuat.get(0, 0, bufferQuat);
			mTrans2.get(0, 0, bufferTrans2);
			mQuat2.get(0, 0, bufferQuat2);
			
			if (patternFound) {
				bufferOpened = true;
			}

			if (bufferOpened) {
					Log.i(TAG, mRot.type() + " ... " + mRot.get(0, 0)[0] + " "
							+ mRot.get(0, 1)[0] + " " + mRot.get(0, 2)[0]);
					/*
					 * double[] buff = new double[(int) (mRot.total() *
					 * mRot.channels())]; mRot.get(0, 0, buff); // local access of
					 * Mat elements from java array
					 */
					
					// data from vision are filled, last two fields are for extraplotated data
					addDataToBuffer(frameTime, bufferRot, bufferTrans, bufferRvec, bufferQuat, bufferTrans2, bufferQuat2);
			}
			break;
		case VIEW_MODE_MSER:
			// return inputFrame.rgba();
			mMat = inputFrame.gray();

			startTime1 = System.nanoTime();
			mFeatureDetector.detect(mMat, mMatOfKeyPoint);
			elapsedTime1 = System.nanoTime() - startTime1;

			Log.i(TAG, "MSER : " + elapsedTime1 / 1000000 + " mils "
					+ ", keypoints: " + mMatOfKeyPoint.toList().size()
					+ ", size: " + mMat.width() + " x " + mMat.height());

			// Imgproc.cvtColor(inputFrame.rgba(), mRgba,
			// Imgproc.COLOR_RGBA2BGRA,4);
			Features2d.drawKeypoints(mMat, mMatOfKeyPoint, mRgba, KEY_COLOR,
					Features2d.DRAW_RICH_KEYPOINTS);
		}
		

		if(touched) {
			
			// writing the img with another thread - can not be seen
            SaveImg(mRgba);
            
            // writing the img in the go - inverted color flashes for a moment
            /*
            if (mRgba.type() == CvType.CV_8UC4) {
           		Imgproc.cvtColor(mRgba, mRgba, Imgproc.COLOR_BGRA2RGB); // CV_8UC4 to CV_8UC3
            }	 	
            Imgcodecs.imwrite(getOutputFile(FILE_IMAGE).toString(), mRgba);
            */
            
            touched = false;
        }
		
		return mRgba;
	}

	
	 // ONTOUCH
	@SuppressLint("SimpleDateFormat")
	@Override
	public boolean onTouch(View v, MotionEvent event) { // when returning true, the touch is repeating immediately
		Log.i(TAG, "onTouch event");
		
		fileIteration++;
		touched = true;
		
		// WTF warning na onTouch
		// http://stackoverflow.com/questions/24952312/ontouchlistener-warning-ontouch-should-call-viewperformclick-when-a-click-is-d
		switch (event.getAction()) {
	    case MotionEvent.ACTION_DOWN:
	        //some code....
	        break;
	    case MotionEvent.ACTION_UP:
	        v.performClick();
	        break;
	    default:
	        break;
	    }
		
		// screenshot by Android API - not dependant on camera preview resolution
        /*
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss");
		String currentDateandTime = sdf.format(new Date());
		String fileName = Environment.getExternalStorageDirectory().getPath()
				+ "/sample_picture_ajio" + currentDateandTime + ".jpg";
		mOpenCvCameraView.takePicture(fileName);
		Toast.makeText(this, fileName + " saved", Toast.LENGTH_SHORT).show();
		*/
		return false;
	}
	
	
	private void SaveImg(final Mat img) {
		// honoroable mentions
        // - image sent to socket in another thread
        // http://stackoverflow.com/questions/23723327/get-mat-object-from-camera-frame-android-for-opencv
        Thread t = new Thread(new Runnable(){

            @Override
            public void run() {
            		
            	 	if (img.type() == CvType.CV_8UC4) {
            	 		Imgproc.cvtColor(img, img, Imgproc.COLOR_BGRA2RGB); // CV_8UC4 to CV_8UC3
            	 	}
                    Imgcodecs.imwrite(getOutputFile(FILE_IMAGE).toString(), img);
                    // http://stackoverflow.com/questions/25059576/highgui-path-is-missing-in-opencv-3-0-0-generated-jar-classnotfoundexception-or

                    mHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(getBaseContext(), "touched & saved ^^ " + fileIteration, Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            });

            t.start();
    }
	 

	/** ----- SAVING DATA ----- */
	public boolean isExternalStorageWritable() {
		String state = Environment.getExternalStorageState();
		if (Environment.MEDIA_MOUNTED.equals(state)) {
			return true;
		}
		Toast.makeText(this, "External Storage is !not! Writable",
				Toast.LENGTH_SHORT).show();
		return false;
	}
	
	public void addDataToBuffer(long bTime, double[] bRot, double[] bTrans, double[] bRvec, double[] bQuat, double[] bTrans2, double[] bQuat2) {
		bufferCameraPosition[counterCam][0] = (double) bTime;
		for (int i = 0; i < 9; i++) {
			bufferCameraPosition[counterCam][i + 1] = bRot[i];
		}
		for (int i = 0; i < 3; i++) {
			bufferCameraPosition[counterCam][i + 10] = bTrans[i];
		}
		for (int i = 0; i < 3; i++) {
			bufferCameraPosition[counterCam][i + 13] = bRvec[i];
		}
		for (int i = 0; i < 4; i++) {
			bufferCameraPosition[counterCam][i + 16] = bQuat[i];
		}
		for (int i = 0; i < 3; i++) {
			bufferCameraPosition[counterCam][i + 20] = bTrans2[i];
		}
		for (int i = 0; i < 4; i++) {
			bufferCameraPosition[counterCam][i + 23] = bQuat2[i];
		}
		if (counterCam < bufferCameraPosition.length - 1) {
			counterCam++;
		} else {
			//writeData(bufferCameraPosition);
			new SaveData().execute(bufferCameraPosition.clone());
			counterCam = 0;
		}
	}
	
	public void addDataToBuffer(long bTime, double[] bRot, double[] bTrans, double[] bRvec, double[] bQuat) {
		bufferCameraPosition[counterCam][0] = (double) bTime;
		for (int i = 0; i < 9; i++) {
			bufferCameraPosition[counterCam][i + 1] = bRot[i];
		}
		for (int i = 0; i < 3; i++) {
			bufferCameraPosition[counterCam][i + 10] = bTrans[i];
		}
		for (int i = 0; i < 3; i++) {
			bufferCameraPosition[counterCam][i + 13] = bRvec[i];
		}
		for (int i = 0; i < 4; i++) {
			bufferCameraPosition[counterCam][i + 16] = bQuat[i];
		}
		if (counterCam < bufferCameraPosition.length - 1) {
			counterCam++;
		} else {
			//writeData(bufferCameraPosition);
			new SaveData().execute(bufferCameraPosition.clone());
			counterCam = 0;
		}
	}

	public void addDataToBuffer(long bTime, double[] bRot, double[] bTrans) {
		bufferCameraPosition[counterCam][0] = (double) bTime;
		for (int i = 0; i < 9; i++) {
			bufferCameraPosition[counterCam][i + 1] = bRot[i];
		}
		for (int i = 0; i < 3; i++) {
			bufferCameraPosition[counterCam][i + 10] = bTrans[i];
		}
		if (counterCam < bufferCameraPosition.length - 1) {
			counterCam++;
		} else {
			//writeData(bufferCameraPosition);
			new SaveData().execute(bufferCameraPosition.clone());
			counterCam = 0;
		}
	}

	public void addDataToBuffer(long bTime, float[] bSensorData, int bSensorType) {
		bufferSensorData[counterSen][0] = (double) bTime;
		bufferSensorData[counterSen][bufferSensorData[0].length - 1] = (double) bSensorType;
		// v pripade cisty sensoz dat (3) se predposledni pole bufferu nevyplni
		// v pripade quaternionu (4) se vyplni vse (ma o prvek vic)
		for (int i = 0; i < bSensorData.length; i++) {
			bufferSensorData[counterSen][i + 1] = bSensorData[i];
		}
		if (counterSen < bufferSensorData.length - 1) {
			counterSen++;
		} else {
			//writeData(bufferSensorData);
			new SaveData().execute(bufferSensorData.clone());
			counterSen = 0;
		}
	}

	/** Write buffer to File */
	public boolean writeData(double[][] buffer) {
		if (buffer[0][0] == Double.MAX_VALUE) {
			return false; // no data will be written down, no file will be created
		}
		
		StringBuilder data = new StringBuilder();
		String floatformat = (buffer[0].length > bufferSensorData[0].length) ? "%.15f "
				: "%.7f ";
		for (int i = 0; i < buffer.length; i++) {
			if (buffer[i][0] == Double.MAX_VALUE) {
				break; // no data will be written down,
			}
			for (int j = 0; j < buffer[0].length; j++) {
				buffer[i][j] = buffer[i][j] < Double.MAX_VALUE ? buffer[i][j] : 0;
				data.append(String.format(floatformat, buffer[i][j]));
			}
			data.deleteCharAt(data.length() - 1); // deletion of the last space on a line
			data.append(System.getProperty("line.separator")); // new line character
		}
		data.deleteCharAt(data.length() - 1); // deletion of the last new line character
		String dataString = data.toString();
		dataString = dataString.replace(",", "."); // 4.0.4 d�lal ve float
													// te�ky, 4.1.2 d�l�
		// ��rky
		File dataFile = (buffer[0].length > bufferSensorData[0].length) ? getOutputFile(FILE_TXT_CAM)
				: getOutputFile(FILE_TXT_SEN);
		if (dataFile == null) {
			Log.d(TAG, "Error creating media file, check storage permissions");
			return false;
		}
		try {
			FileWriter writer = new FileWriter(dataFile, true);
			writer.append(dataString);
			writer.append(System.getProperty("line.separator"));
			// \n neni vid�t v notepadu, aha tohle taky ne...
			writer.flush();
			writer.close();
		} catch (Exception e) {
			Log.d(TAG, "Error writing datafile.txt");
		}
		return true;
	}
	
	/*
	 * This method will trigger if buffer for sensor data or buffer for camera data is full.
	 * However, usage of this async task might lead to unexpected timestamps shifts and
	 * bad written data. Therefore initialization of big enough buffer (for 1 minute execution)
	 * is very easy workaround - how to avoid writing down data simuntaneously with recording. 
	 * */
	private class SaveData extends AsyncTask<double[][], Void, Void>{

		@Override
		protected Void doInBackground(double[][]... params) {
		    // your background code here. Don't touch any UI components
			Log.d(TAG, "Async task running...");
			writeData(params[0]);
			/*
		    if(writeData(params[0]))
		        return true;                
		    else
		        return false;
		    */
			return null;
		}

		protected void onPostExecute(Boolean result) {
		     //This is run on the UI thread so you can do as you wish here
		     if(result) {
		    	 Toast.makeText(HelloOpenCvActivity.this, "Data zaps�na.",
		 				Toast.LENGTH_SHORT).show();
		     } else {
		    	 Toast.makeText(HelloOpenCvActivity.this, "Z�pis selhal.",
			 				Toast.LENGTH_SHORT).show();
		     }
		         
		 }

		}
	

	/** Create a File for saving an image or video */
	private static File getOutputFile(int type) {
		File storageDir = new File(
				Environment
						.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
				myFileDir);
		// Create the storage directory if it does not exist
		if (!storageDir.exists()) {
			if (!storageDir.mkdirs()) {
				Log.d(TAG, "failed to create directory");
				return null;
			}
		}
		// Create a media file name
		File outFile = null;
		if (type == FILE_TXT_CAM) {
			outFile = new File(storageDir.getPath() + File.separator
					+ fileTimeStamp + "_c.txt");
		} else if (type == FILE_TXT_SEN) {
			outFile = new File(storageDir.getPath() + File.separator
					+ fileTimeStamp + "_s.txt");
		} else if (type == FILE_IMAGE) {
			outFile = new File(storageDir.getPath() + File.separator
					+ "IMG_" + fileTimeStamp + "_" + String.format("%03d", fileIteration) + ".jpg");
		}
		return outFile;
	}

	/** ----- NATIVE METHODS ----- */
	public native boolean FindFeatures(long matAddrGr, long matAddrRgba,
			long matAddrRot, long matAddrTrans, long matAddrRvec, long matAddrQuat, long matAddrSensQuat,
			long matAddrTrans2, long matAddrQuat2);
	
	
	
	

}
