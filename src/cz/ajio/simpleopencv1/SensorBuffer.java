package cz.ajio.simpleopencv1;

import org.opencv.core.CvType;
import org.opencv.core.Mat;

import android.R.bool;
import android.util.Log;

public class SensorBuffer {
	/*
	 * Buffer for storing temporary sensor data before they are handed to c++ part for fusion.
	 * */
	
	private float[] oriQuaternion = new float[4];
	private Mat oriQuaternionMat;
	private long sampleTime;
	private Boolean ready;
	
	
	public SensorBuffer() {
		super();
		ready = false;
		oriQuaternionMat = new Mat();
		oriQuaternionMat.create(4,1,CvType.CV_32F); // as we expect to store float in it - type CV_32F
	}

	
	public Boolean isReady() {
		return ready;
		
	}
	
	public synchronized Mat getOriQuaternionMat(long frameTime) {
		/*Log.d("whatever","java buff mSensQuat "
				+ oriQuaternion[0] + " " + oriQuaternion[1] + " " + oriQuaternion[2] + " " + oriQuaternion[3]);
		Log.d("whatever","diff between frame and sample (" + frameTime + "-" + sampleTime + "): " 
				+ ((frameTime - sampleTime)/(float)1000000) + " milis");*/
		oriQuaternionMat.put(0, 0, oriQuaternion);
		return oriQuaternionMat;
	}
	

	public synchronized float[] getOriQuaternion() {
		return oriQuaternion;
	}

	public synchronized void setOriQuaternion(float[] oriQuaternion, long sampleTime) {
		this.oriQuaternion = oriQuaternion.clone();
		this.sampleTime = sampleTime;
		ready = true;
	}
	
	

}
