package cz.ajio.simpleopencv1;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

public class SensorTestActivity extends Activity {
	
	public static final String TAG = "Ajio_DPproject";
	
	private SensorManager mSensorManager;
	private Sensor mSensorLinearAcceleration;
	private Sensor mSensorGyroscope;
	
	private TextView mTextViewDataGyro;
	private TextView mTextViewDataLinAcc;
	
	private long lastEventTime = System.nanoTime();
	private int measuresPerInterval = 500;
	private int counter = 0;
	
	private final SensorEventListener mSensorListener = new SensorEventListener() {
		@Override
		public void onSensorChanged(SensorEvent event) {
			switch (event.sensor.getType()) {
			case Sensor.TYPE_GYROSCOPE:
				mTextViewDataGyro.setText(String.format("" +
						"Gyro \n %.2f \n %.2f \n %.2f", event.values[0],
						event.values[1], event.values[2]));
				break;
			case Sensor.TYPE_LINEAR_ACCELERATION:
				if (counter == 0 || counter == measuresPerInterval) {
					// �as v sec pro 1000 vzork�
					//String s = String.valueOf("linacc time: "+(event.timestamp-lastEventTime)/1000000000.0);
					// �as f v Hz z 1000 vzork�
					String s = String.valueOf("linacc freq: "+
					((1000000000.0*measuresPerInterval)/(event.timestamp-lastEventTime)) + " Hz");
					lastEventTime = event.timestamp;
		            Log.i(TAG, s);
		            counter = 0;
				}
				counter++;
				
				mTextViewDataLinAcc.setText(String.format("" +
						"LinAcc \n %.2f \n %.2f \n %.2f", event.values[0],
						event.values[1], event.values[2]));
				mTextViewDataLinAcc.setBackgroundColor(valueToColor(event.values[2]));
				break;
			default:
				// hazelo to ten toast porad
				// ("Neplatn� senzor p�i sn�m�n� dat pro orientaci!");
				break;
			}
		}
		@Override
		public void onAccuracyChanged(Sensor sensor, int accuracy) {
			// TODO Auto-generated method stub
		}

		
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.sensor_test_layout);
		
		mTextViewDataGyro = (TextView) findViewById(R.id.data_gyro);
		mTextViewDataLinAcc = (TextView) findViewById(R.id.data_linacc);
		
		mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        mSensorLinearAcceleration = mSensorManager.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION);
        mSensorGyroscope = mSensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.sensor_test, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	
	@Override
	protected void onResume() {
		super.onResume();
		
		if (!mSensorManager.registerListener(mSensorListener, mSensorLinearAcceleration,
				SensorManager.SENSOR_DELAY_FASTEST)) {
			Toast.makeText(this, "linacc error", Toast.LENGTH_SHORT).show();
		}
		if (!mSensorManager.registerListener(mSensorListener, mSensorGyroscope,
				SensorManager.SENSOR_DELAY_FASTEST)) {
			Toast.makeText(this, "gyro error", Toast.LENGTH_SHORT).show();
		}
	}
	
	@Override
	protected void onPause() {
		super.onPause();
		
		mSensorManager.unregisterListener(mSensorListener);
	}
	
	
	private int valueToColor(double value) {
		// pokus o p�epo�et intervalu 0 a� N na barvu
		value = value/9.81*150;
		int maxval = 150; // min 0
		int r, g, b;
		value = value > maxval ? 0 : (maxval - value) / 100;
		r = (int) (510 - (value * 255 * 2));
		r = r > 255 ? 255 : r;
		r = r < 0 ? 0 : r;
		g =  (int) (value * 255 * 2);
		g = g > 255 ? 255 : g;
		b = (int) ((value - 1) * 255 * 2);
		b = b < 0 ? 0 : b;
		return Color.rgb(r, g, b);
	}
	
}
