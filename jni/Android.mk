LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

include C:\Work\OpenCV-3.0.0-android-sdk\sdk\native\jni/OpenCV.mk # you need to set it here also !!! and in onResume

LOCAL_MODULE    := L_detect
LOCAL_SRC_FILES := jni_part.cpp
LOCAL_LDLIBS +=  -llog -ldl

include $(BUILD_SHARED_LIBRARY)
