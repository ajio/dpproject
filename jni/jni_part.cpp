#include <jni.h>
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/features2d/features2d.hpp>
#include <vector>
#include <queue>

#include <opencv2/opencv.hpp> // pridanim tohoto funguje cout

//#include "white_crc.hpp" // neni potreba - hlasi duplicitni definice

// DEBUG pro native - mozno hodit ven do headeru
#include <android/log.h>
#define  LOG_TAG    "NDK-JNI_ajio"
#define  LOGD(...)  __android_log_print(ANDROID_LOG_DEBUG, LOG_TAG, __VA_ARGS__)
#define  LOGE(...)  __android_log_print(ANDROID_LOG_ERROR, LOG_TAG, __VA_ARGS__)
// If you want you can add other log definition for info, warning etc

using namespace std;
using namespace cv;


// timer
typedef unsigned long long timestamp_t;
static timestamp_t
get_timestamp ()
{
  struct timeval now;
  gettimeofday (&now, NULL);
  return  now.tv_usec + (timestamp_t)now.tv_sec * 1000000; // microsec
}

// matlab like modulo for negative ints
int modulo (int a, int b) { return a >= 0 ? a % b : ( b - abs ( a%b ) ) % b; }

// quaternion normalization
void quatnorm(Mat &q) {
	double w = q.at<double>(0);
	double x = q.at<double>(1);
	double y = q.at<double>(2);
	double z = q.at<double>(3);
	q /= sqrt(w*w + x*x + y*y + z*z);
}

// quaternion multiplication (it is noncommutative)
Mat quatmulti(Mat q, Mat p) {
	Mat out;
	out.create(4, 1, CV_64F);
	out.at<double>(0) = q.at<double>(0)*p.at<double>(0) - q.at<double>(1)*p.at<double>(1) - q.at<double>(2)*p.at<double>(2) - q.at<double>(3)*p.at<double>(3);
	out.at<double>(1) = q.at<double>(1)*p.at<double>(0) + q.at<double>(0)*p.at<double>(1) - q.at<double>(3)*p.at<double>(2) + q.at<double>(2)*p.at<double>(3);
	out.at<double>(2) = q.at<double>(2)*p.at<double>(0) + q.at<double>(3)*p.at<double>(1) + q.at<double>(0)*p.at<double>(2) - q.at<double>(1)*p.at<double>(3);
	out.at<double>(3) = q.at<double>(3)*p.at<double>(0) - q.at<double>(2)*p.at<double>(1) + q.at<double>(1)*p.at<double>(2) + q.at<double>(0)*p.at<double>(3);
	/*LOGD("QUATTEST q %f %f %f %f ",
				q.at<double>(0), q.at<double>(1), q.at<double>(2), q.at<double>(3));
	LOGD("QUATTEST p %f %f %f %f ",
				p.at<double>(0), p.at<double>(1), p.at<double>(2), p.at<double>(3));
	LOGD("QUATTEST out %f %f %f %f ",
			out.at<double>(0), out.at<double>(1), out.at<double>(2), out.at<double>(3));*/
	return out;
}

// quaternion inversion, in case of unit quaternion, inversion = conjugate = change sign of the axis
void quatinv(Mat &q) {
	q.at<double>(1) = -q.at<double>(1);
	q.at<double>(2) = -q.at<double>(2);
	q.at<double>(3) = -q.at<double>(3);
}

// transformation from OCV rotation vector 'rvec' (angle axis) to quaternion
void rvec2quat(Mat &rvec, Mat &quat) {
// http://euclideanspace.com/maths/geometry/rotations/conversions/angleToQuaternion/index.htm
// http://stackoverflow.com/questions/12053895/converting-angular-velocity-to-quaternion-in-opencv

	// https://github.com/Itseez/opencv/blob/a18183554995f66d15a21929da1f00edbb71b385/modules/calib3d/src/solvepnp.cpp
	quat.create(4,1,CV_64F);

	const float delta = 0.0001f;
	double x = rvec.at<double>(0);
	double y = rvec.at<double>(1);
	double z = rvec.at<double>(2);
	const float angle = sqrt(x*x + y*y + z*z);
	//LOGD("axis: %f %f %f | angle: %f ",x,y,z,angle);
	if ( angle != INFINITY) {
		x /= angle; y /= angle; z /= angle;

		const float s = sin(angle/2.0f);
		//LOGD("norm .. axis: %f %f %f | angle: %f | s: %f | type: %d ",x,y,z,angle,s,rvec.type());
		quat.at<double>(0) = cos(angle/2.0f);
		quat.at<double>(1) = x*s;
		quat.at<double>(2) = y*s;
		quat.at<double>(3) = z*s;

		/*LOGD("rvec normalized? value: %f",x*x + y*y + z*z);
		if (((x*x + y*y + z*z) - 1.0f) > delta) {
			LOGE("rvec norm failed");
		}
		LOGD("quat normalized? value: %f",quat.at<double>(0)*quat.at<double>(0) +
				quat.at<double>(1)*quat.at<double>(1) +
				quat.at<double>(2)*quat.at<double>(2) +
				quat.at<double>(3)*quat.at<double>(3));*/
	} else {
		//LOGE("angle is inf");
	}
}

// transformation from quaternion to OCV rotation vector 'rvec' (angle axis)
void quat2rvec(Mat &quat, Mat &rvec) {
	rvec.create(3,1,CV_64F);
	if (quat.at<double>(0) > 1) {
		quatnorm(quat); // if w>1 acos and sqrt will produce errors, this cant happen if quaternion is normalised
	}
	double qw = quat.at<double>(0);
	double qx = quat.at<double>(1);
	double qy = quat.at<double>(2);
	double qz = quat.at<double>(3);
	double x, y, z;
    double angle = 2 * acos(qw);
    double s = sqrt(1 - qw*qw); // assuming quaternion normalised then w is less than 1, so term always positive.
    if (s < 0.0001f) { // test to avoid divide by zero, s is always positive due to sqrt
    	// if s close to zero then direction of axis not important
    	x = 1; // if it is important that axis is normalised then replace with x=1; y=z=0;
    	y = 0; // ... yup, replaced, originaly x = qx, y = qy, z = qz
    	z = 0;
    } else {
    	x = qx / s; // normalise axis
    	y = qy / s;
    	z = qz / s;
    }
    rvec.at<double>(0) = x;
    rvec.at<double>(1) = y;
    rvec.at<double>(2) = z;
    rvec *= angle;
}


// struktura vytahnuta z white_crc headru
typedef struct {
	Point2d cen;
	int size;
	int min_c;
} center;



//#define DBG

#ifdef DBG
static Mat dbg;

#endif

static int max_r, max_c;
bool dbg_flag;
static vector<center> *glob_out;

static int find_seg_line(unsigned char *row, int p, int row_ptr, Mat &i, Mat &buf, int &white_lvl, int &white_cnt, int &lef, int &rig, int &black_lvl) {
	int cumul_sum, cumul_cnt, black;
	int black_min_l, black_min_r;
	int white_max=0, white_max_s=0, white_min=256;
	double white_avg=white_lvl;
	int white_sum=0, white_local_cnt=0, white_lvl_init=white_lvl, white_cnt_init=white_cnt;
	int pix_lvl;
	int ret = 0;
	int sum_dif;
	unsigned char *b = buf.ptr(row_ptr);

	pix_lvl = (white_lvl*10)/100;
	rig = p+1;
	b[p]=120;
	white_min = white_max = row[p];
	do {
		while (rig<max_r && row[rig]>=white_lvl-pix_lvl && row[rig]<=white_lvl+2*pix_lvl && b[rig]==0) {
			white_avg = (white_avg * white_cnt + row[rig]) / (white_cnt + 1.0);
			white_lvl = white_avg;
			white_sum += row[rig];
			white_local_cnt++;
			if (row[rig]>white_max) {
				white_max = row[rig];
			}
			if (row[rig]<white_min) {
				white_min = row[rig];
			}
			white_cnt++;
			b[rig]=120;
			rig++;
		}
		cumul_sum = 0;
		cumul_cnt = 0;
		sum_dif = (white_lvl-60)*0.56;
		if (row[rig]>white_lvl+2*pix_lvl) {
			black = 0;
			white_max_s = row[rig];
			while (rig<max_r && row[rig]>white_lvl+pix_lvl && cumul_sum<sum_dif && b[rig]==0) {
				cumul_sum += (row[rig]-white_lvl)-pix_lvl;
				cumul_cnt++;
				rig++;
				if (row[rig]>white_max_s) {
					white_max_s = row[rig];
				}
			}
#ifdef DBG
			cout << "White cumul right " << cumul_sum << " cnt " << cumul_cnt << " max " << white_max_s << endl;
#endif
		} else {
			black = 1;
			black_min_r = row[rig];
			while (rig<max_r && row[rig]<white_lvl-pix_lvl && cumul_sum<sum_dif && b[rig]==0) {
				cumul_sum += (white_lvl)-row[rig];
				cumul_cnt++;
				rig++;
				if (row[rig]< black_min_r) {
					black_min_r = row[rig];
				}
			}
#ifdef DBG
			cout << "Black cumul right " << cumul_sum << " cnt " << cumul_cnt << endl;
#endif
		}
	} while (rig<max_r && cumul_sum<sum_dif  && b[rig]==0);

	if (black==0 || rig >=max_r) {
		ret = 0;
	} else {
		lef = p-1;
		do {
			while (lef>0 && row[lef]>=white_lvl-pix_lvl && row[lef]<=white_lvl+2*pix_lvl&& b[lef]==0) {
				white_avg = (white_avg * white_cnt + row[lef]) / (white_cnt + 1.0);
				white_lvl = white_avg;
				white_sum += row[lef];
				white_local_cnt++;
				if (row[lef]>white_max) {
					white_max = row[lef];
				}
				if (row[lef]<white_min) {
					white_min = row[lef];
				}
				white_cnt++;
				b[lef]=120;
				lef--;
			}
			cumul_sum = 0;
			cumul_cnt = 0;
			sum_dif = (white_lvl-60)*0.56;
			if (row[lef]>white_lvl+2*pix_lvl&& b[lef]==0) {
				black = 0;
				white_max_s = row[lef];
				while (lef>0 && row[lef]>white_lvl+pix_lvl && cumul_sum<sum_dif && b[lef]==0) {
					cumul_sum += (row[lef]-white_lvl)-pix_lvl;
					cumul_cnt++;
					lef--;
					if (row[lef]>white_max_s) {
						white_max_s = row[lef];
					}
				}
#ifdef DBG
				cout << "White cumul left " << cumul_sum << " cnt " << cumul_cnt << endl;
#endif
			} else {
				black = 1;
				black_min_l = row[lef];
				while (lef>0 && row[lef]<white_lvl-pix_lvl && cumul_sum<sum_dif && b[lef]==0) {
					cumul_sum += (white_lvl)-row[lef];
					cumul_cnt++;
					lef--;
					if (row[lef]< black_min_l) {
						black_min_l = row[lef];
					}
				}
#ifdef DBG
				cout << "Black cumul left " << cumul_sum << " cnt " << cumul_cnt << endl;
#endif
			}
		} while (lef>0 && cumul_sum<sum_dif && b[lef]==0);
		ret = (lef>0 && black==1)?(rig-lef-2):0;
	}
	black_lvl = (black_min_l+black_min_r)/2.0;
	if (dbg_flag) {
		cout << "line "<< row_ptr << "Left " << lef << " right " << rig << " zacatek " << p<< " white " <<  white_lvl << " min "<<white_min << " max " << white_max<< " black lvl " << black_lvl << " pix lvl "<< pix_lvl << " ret " << ret << endl;
	}
	return ret;
}

void grow_circle2(unsigned char *row, int p, int row_ptr, Mat &i, Mat &buf) {
	int white_lvl, white_cnt;
	int black_lvl;
	int left, right;
	int left_old, right_old;
	int left_1, right_1;
	int seg_size, line_size, old_line_size;
	int r = row_ptr, c, ii;
	bool correct=1;
	unsigned char *row_c;
	unsigned char *last_row;
	unsigned char *buf_r;
	double cen_x, cen_y;
	int left_mode = 0, right_mode=0;
	int row_bot=0, row_top=0;
	center cent;

	white_lvl = row[p];
	white_cnt=1;

	seg_size = old_line_size = line_size = find_seg_line(row, p, row_ptr, i, buf, white_lvl, white_cnt, left, right, black_lvl);
	if (seg_size>0) {
		left_1 = left;
		right_1 = right;
		cen_x = (left+right)/2.0;
		cen_y = row_ptr;
		row_c = row;
		buf_r = buf.ptr(r);
		while (line_size>0 && r<max_c-1 && correct) { // go down
			left_old = left;
			right_old = right;
			r++;
			last_row = row_c;
			row_c = i.ptr(r);
			c = (left+right)/2;
			if (row_c[c]<(white_lvl*88)/100) {
				line_size = 0;
				for (c=left+1; correct && (c<right); c++) {
					correct = (row_c[c]<(white_lvl*88)/100);
				}
			} else {
			    line_size = find_seg_line(row_c, c, r, i, buf, white_lvl, white_cnt, left, right, black_lvl);
			    if (line_size<=0) {
			    	correct = 0;
			    }
//			    lvl = (black_lvl+white_lvl)/2.0;
			}
			if (line_size>0) {
				if (left<left_old-2) {
					correct = (left_mode<2);
					left_mode=1;
				}
				if (left>left_old+2) {
					left_mode=2;
				}
				if (right>right_old+2) {
					correct = (right_mode<2);
					right_mode=1;
				}
				if (right<right_old-2) {
					right_mode=2;
				}
/*				if (left<left_old-1) {
					for (ii = left; ii<left_old-1 && correct; ii++) {
						col = last_row[ii];
						correct = correct && (col < lvl);
						cout << " left " << r << " c-1 col " << col << " cor " << correct << " lvl "<< lvl << endl;
					}
				} else if (left_old < left-1) {
					for (ii = left_old; ii<left-1 && correct; ii++) {
						col = row_c[ii];
						correct = correct && (col < lvl);
						cout << " left " << r << " c col " << col << " cor " << correct << " lvl "<< lvl << endl;
					}
				}
				if (right<right_old-1) {
					for (ii = right+2; ii<=right_old && correct; ii++) {
						col = row_c[ii];
						correct = correct && (col < lvl);
						cout << " right " << r << " c col " << col << " cor " << correct << " lvl "<< lvl << endl;
					}
				} else if (right_old < right-1) {
					for (ii = right_old+2; ii<=right && correct; ii++) {
						col = last_row[ii];
						correct = correct && (col < lvl);
						cout << " right " << r << " c-1 col " << col << " cor " << correct << " lvl "<< lvl << endl;
					}
				}*/
			}
			if (correct) {
				cen_y = (cen_y * seg_size + r * line_size)/(seg_size+line_size);
				cen_x = (cen_x * seg_size + ((left+right)/2.0) * line_size)/(seg_size+line_size);
				seg_size += line_size;
			}
#ifdef DBG
			cout << "Line "<<r<<" correct "<<correct<< " line size "<<line_size<<endl;
#endif
		}
		row_bot = r;
		r=row_ptr;
		left = left_1;
		right = right_1;
		row_c = i.ptr(r);
		line_size = old_line_size;
		left_mode = 0; right_mode=0;
		while (line_size>0 && r>0 && correct) { // go up
			left_old = left;
			right_old = right;
			r--;
			last_row = row_c;
			row_c = i.ptr(r);
			c = (left+right)/2;
			if (row_c[c]<(white_lvl*88)/100) {
				line_size = 0;
				for (c=left+1; correct && (c<right); c++) {
					correct = (row_c[c]<(white_lvl*88)/100);
				}
			} else {
			    line_size = find_seg_line(row_c, c, r, i, buf, white_lvl, white_cnt, left, right, black_lvl);
			    //lvl = (black_lvl+white_lvl)/2.0;
			    if (line_size<=0) {
			    	correct = 0;
			    }
			}
			if (line_size>0) {
				if (left<left_old-2) {
					left_mode=2;
				}
				if (left>left_old+2) {
					correct = (left_mode<2);
					left_mode=1;
				}
				if (right>right_old+2) {
					right_mode=2;
				}
				if (right<right_old-2) {
					correct = (right_mode<2);
					right_mode=1;
				}
				/*
				if (left<left_old-1) {
					for (ii = left; ii<left_old-1 && correct; ii++) {
						col = last_row[ii];
						correct = correct && (col < lvl);
						cout << " left " << r << " c-1 col " << col << " cor " << correct << " lvl "<< lvl << endl;
					}
				} else if (left_old < left-1) {
					for (ii = left_old; ii<left-1 && correct; ii++) {
						col = row_c[ii];
						correct = correct && (col < lvl);
						cout << " left " << r << " c col " << col << " cor " << correct << " lvl "<< lvl << endl;
					}
				}
				if (right<right_old-1) {
					for (ii = right+2; ii<=right_old && correct; ii++) {
						col = row_c[ii];
						correct = correct && (col < lvl);
						cout << " right " << r << " c col " << col << " cor " << correct << " lvl "<< lvl << endl;
					}
				} else if (right_old < right-1) {
					for (ii = right_old+2; ii<=right && correct; ii++) {
						col = last_row[ii];
						correct = correct && (col < lvl);
						cout << " right " << r << " c-1 col " << col << " cor " << correct << " lvl "<< lvl << endl;
					}
				}
				*/
			}
			if (correct) {
				cen_y = (cen_y * seg_size + r * line_size)/(seg_size+line_size);
				cen_x = (cen_x * seg_size + ((left+right)/2.0) * line_size)/(seg_size+line_size);
				seg_size += line_size;
			}
#ifdef DBG
			cout << "Line>"<<r<<"< correct "<<correct<< " line size "<<line_size<<endl;
#endif
		}
		row_top = r;
		cent.cen.x = cen_x;
		cent.cen.y = cen_y;
		cent.size = seg_size;
		//TODO cent.min_c = min_color;
		correct = correct && (seg_size>8);
		if (correct && (row_bot-row_top)>3) {
			center cc;
#ifdef DBG
			cout << "Founded " << cen_x << "," << cen_y << " size " << seg_size << " row bot " << row_bot << " top "<< row_top<< endl;
#endif
			cc.cen.x = cen_x;
			cc.cen.y = cen_y;
			buf_r = buf.ptr((int)cen_y);
			buf_r[(int)cen_x]=255;
			cc.size = seg_size;
			cc.min_c = white_lvl;
			glob_out->push_back(cc);
		}
		//cout << "Grow " << cen_x << "," << cen_y << " size " << seg_size << endl;
	}
}


int find_local_max(Mat &i, Mat &buf) {
	int num=0;
	int x, y, lx, ly, s;
	int cont;
	unsigned char *l;
	unsigned char *b;

	for (x=0; x<max_c; x++) {
		b = buf.ptr(x);
		l = i.ptr(x);
		for (y=2; y<max_r-2; y++) {
			if (b[y]==0) {
				while (y<max_r-2 && (l[y-1]>=l[y] || l[y-2]>=l[y] || b[y]!=0)) {
					y++;
				}
				do {
				   cont=0;
				   while (y<max_r-2 && l[y+1]>=l[y]) {
					   y++;
				   }
				   if (y<max_r-2 &&l[y+2]>=l[y]) {
					   y+=2;
					   cont=1;
				   }
				} while(y<max_r-2 && cont==1);

				if (y<max_r-2 && l[y]>90 && b[y]==0) {
					grow_circle2(l, y, x, i, buf);
#ifdef DBG
				//imshow("buf", buf);
				//imshow("dbg", dbg);
				//waitKey(0);
#endif
				}
			}
		}
	}

	return num;
}

int find_centers_my3(Mat &i, vector<center> &out, Mat &buf) {
	dbg_flag = false;
#ifdef DBG
	dbg = i.clone();
	dbg_flag = true;
	dbg_key = 0;
#endif
	unsigned char *line_buf;

	glob_out = &out;
	max_r = i.cols;
	max_c = i.rows;
#if DBG
	cout << "My3 init max cols " << max_r << " max rows "<< max_c<< endl;
#endif
	buf.create(i.rows, i.cols, CV_8U);
	for (int line_ptr = 0; line_ptr < max_c; line_ptr ++) {
		line_buf = buf.ptr(line_ptr);
		memset(line_buf, 0, max_r);
	}

	find_local_max(i, buf);
#if DBG
	cout << "Nalezeno "<< out.size() << "maxim"<<endl;

				imshow("buf", buf);
				imshow("dbg", dbg);
				waitKey(0);
#endif
}

int find_centers(Mat &i, vector<center> &out, Mat &buf) {
	int ret;
	// vyber funkce pro detekci center
	//return find_centers_MSER(i, out, buf);
	//double t1 = (double) getTickCount();
	ret = find_centers_my3(i, out, buf);
	//double t2 = (double) getTickCount();
    //cout << "Center search "<< ((t2-t1 )* 1000. / getTickFrequency()) << endl;
    return ret;
}

static const Vec3b bcolors[] = { Vec3b(0, 0, 255), Vec3b(0, 255, 0), Vec3b(0,
		255, 255), Vec3b(255,0 , 0), Vec3b(255, 128, 0), Vec3b(255, 255, 0),
		Vec3b(0, 128, 255), Vec3b(255, 0, 255), Vec3b(255, 255, 255) };

void cross (Mat *img, Point pp, int c) {
	for (int j = -4; j < 5; j++) {
		Point p = pp;
		p.x+=j;
		img->at<Vec3b>(p) = bcolors[c % 9];
		p.y+=1;
		img->at<Vec3b>(p) = bcolors[c % 9];
		p.y-=2;
		img->at<Vec3b>(p) = bcolors[c % 9];
	}
	for (int j = -4; j < 5; j++) {
		Point p = pp;
		p.y+=j;
		img->at<Vec3b>(p) = bcolors[c % 9];
		p.x+=1;
		img->at<Vec3b>(p) = bcolors[c % 9];
		p.x-=2;
		img->at<Vec3b>(p) = bcolors[c % 9];
	}
}

void dr_point(Mat &img, Point pp, int c) {
	for (int j = -3; j < 4; j++) {
		Point p = pp;
		p.x+=j;
		if (p.x>1 && p.x<img.cols-2 && p.y>1 && p.y<img.rows-2) {
			img.at<Vec3b>(p) = bcolors[c % 9];
		}
	}
	for (int j = -3; j < 4; j++) {
		Point p = pp;
		p.y+=j;
		if (p.x>1 && p.x<img.cols-2 && p.y>1 && p.y<img.rows-2) {
			img.at<Vec3b>(p) = bcolors[c % 9];
		}
	}
}

#define DBG_SEG -2
// tan 22.5 stupnu
#define tan 0.414213562373
// smernice 16 paprsku po 22.5 stupne
//                               0    1  2    3  4     5   6    7   8     9  10   11  12   13  14    15
static const double usec_x[] = { 1, 1, 1, tan, 0, -tan, -1, -1, -1, -1, -1,
		-tan, 0, tan, 1, 1 };
static const double usec_y[] = { 0, tan, 1, 1, 1, 1, 1, tan, 0, -tan, -1, -1,
		-1, -1, -1, -tan };

#define siz1 1.08239220029
#define siz2 1.41421356237

static const double usec_size[] = { 1, siz1, siz2, siz1, 1, siz1, siz2, siz1, 1,
		siz1, siz2, siz1, 1, siz1, siz2, siz1 };

static const Point shift_p[4] = { { 1, 0 }, { 0, 1 }, { -1, 0 }, { 0, -1 } };

// velikost polomeru v pixelech   6   7    8    9   10   11   12   13   14   15   16   17
static const int lim_tlu[12] = { 200, 200, 220, 250, 290, 300, 310, 320, 330,
		340, 350, 360 };
static const int lim_ten[12] = { 180, 180, 210, 230, 270, 280, 290, 300, 310,
		320, 330, 340 };
#define N_PATTERN 4

// dalo by se zkusit oprit i o jine pomery delek, mozna stabilnejsi
static const double pattern[][2] = { { 0.379, 0.789 }, { 0.789, 0.379 }, {
		0.304, 0.304 }, { 1.0, 1.0 } };

// kolik pixelu je sum - to je take trochu problematicke, pri rozmazanem obrazku je nekdy ta seda hodne blizko bile
#define PIX_LVL 30

// tolerance plochy oblasti ku plose obsaneho obdelniku
#define circTol 0.9

// maximum pro detekci nenastaveni vystupu
#define DEF_MAX 1000000

// presnost detekce pomeru velikosti bile a cerne v pixelech, pro vetsi obrazek je vetsi tolerance
#define PIXEL_SIZE1 4
#define PIXEL_SIZE2 3

static inline bool aprox(int a, int b, int scale) {
	bool r;
	if (scale > 30) {
		r = ((a + PIXEL_SIZE1) > b) && ((a - PIXEL_SIZE1) < b);
	} else {
		r = ((a + PIXEL_SIZE2) > b) && ((a - PIXEL_SIZE2) < b);
	}
	return r;
}
static inline double sqr(double a) {
	return a * a;
}

// pro ulozeni intenzit jednoho paprsku
static int c_mem[2048];

// detekce jednoho paprsku
// vystupem je 8-9 indexu zmen z bile do cerne zpet do bile, do cerne a nakonec do bile - output
// k tomu suma ploch pod urovni bile barvy - 2 - black_sum
static int detect_line(Mat &m, double centx, double centy, int line_type,
		int minc, int *output, int *black_sum, int ii) {
	double p_x, p_y;
	Point p;
	int size_l;
	double white_lvl, black_lvl;
	int c, c2;
	int pix_lvl = PIX_LVL;
	int white_cnt, black_cnt, last_c;
	int tmp_ptr, block_ptr;
	int ptr_out = 0, max_len, line_mode;
	int ptr_sum = 0;
	int filter = 0;
	black_sum[0] = black_sum[1] = 0;
	p_x = centx + usec_x[line_type];
	p_y = centy + usec_y[line_type];
	size_l = 1;
	p.x = p_x;
	p.y = p_y;
	max_len = DEF_MAX;
	if (p.x > 0 && p.y > 0 && p.x < m.cols && p.y < m.rows) {
		c_mem[size_l] = c = m.at<unsigned char>(p);
		if (ii == DBG_SEG) {
			cout << c << ",";
		}
		white_lvl = c;
		white_cnt = 1;
		last_c = c;
		line_mode = 3;
		while (c >= 0 && size_l < max_len && ptr_out < 8) {
			size_l++;
			p_x += usec_x[line_type];
			p_y += usec_y[line_type];
			p.x = p_x;
			p.y = p_y;
			if (p.x > 0 && p.y > 0 && p.x < m.cols - 1 && p.y < m.rows - 1) {
				c = m.at<unsigned char>(p);
				if (ii == DBG_SEG) {
					cout << "," << c;
				}
				if (line_type == 2 || line_type == 6 || line_type == 10
						|| line_type == 14) {
					// uhloprickove paprsky mohou minout tmavou oblast, proto se prumeruje se sousednim pixelem
					c2 = m.at<unsigned char>(p + shift_p[(line_type - 2) / 4]);
					c = (c + c2) / 2;
					if (ii == DBG_SEG) {
						cout << "(" << c2 << "," << c << ")";
					}
				}
				c_mem[size_l] = c;
				if (line_mode == 0) { // from white to black
					if (c < white_lvl - pix_lvl) {
						black_cnt += white_lvl - c;
					}
					if (last_c <= c) {
						black_lvl = last_c;
						pix_lvl = (white_lvl - black_lvl) / 4;
						if (ii == DBG_SEG) {
							cout << " pix lvl " << pix_lvl << "white lvl "
									<< white_lvl << "black_lvl" << black_lvl
									<< endl;
						}
						tmp_ptr = size_l - 2;
						while (c_mem[tmp_ptr] < black_lvl + pix_lvl
								&& tmp_ptr > block_ptr) {
							tmp_ptr--;
						}
						if (black_lvl + pix_lvl < c) {
							output[ptr_out++] = tmp_ptr + 1;
							if (filter) {
								line_mode = 1; // keep black
								black_lvl = (last_c + c) / 2;
							} else {
								output[ptr_out++] = tmp_ptr + 1;
								block_ptr = size_l;
								line_mode = 2; // from black to white
							}
						} else {
							output[ptr_out++] = tmp_ptr + 1;
							line_mode = 1; // keep black
						}
					}
				} else if (line_mode == 1) { // keep black
					if (c < white_lvl - pix_lvl) {
						black_cnt += white_lvl - c;
					}
					if (c >= black_lvl + pix_lvl) {
						output[ptr_out++] = size_l - 1;
						block_ptr = size_l;
						line_mode = 2;
					}
				} else if (line_mode == 2) { // from black to white
					if (last_c >= c || last_c >= white_lvl - pix_lvl) {
						black_sum[ptr_sum++] = black_cnt;
						white_lvl = (white_lvl * white_cnt + last_c)
								/ (white_cnt + 1);
						white_cnt++;
						tmp_ptr = size_l - 2;
						while (c_mem[tmp_ptr] > white_lvl - pix_lvl
								&& tmp_ptr > block_ptr) {
							tmp_ptr--;
						}
						if (white_lvl - pix_lvl > c) {
							output[ptr_out++] = tmp_ptr + 1;
							if (filter) {
								line_mode = 3; // keep black
							} else {
								output[ptr_out++] = tmp_ptr + 1;
								block_ptr = size_l;
								black_cnt = white_lvl - c;
								line_mode = 0; // from white to black
							}
						} else {
							output[ptr_out++] = tmp_ptr + 1;
							line_mode = 3; // keep white
						}
					} else {
						if (c < white_lvl - pix_lvl) {
							black_cnt += white_lvl - c;
						}
					}
				} else if (line_mode == 3) { // keep white
					if (c > white_lvl - pix_lvl) {
						white_lvl = (white_lvl * white_cnt + c)
								/ (white_cnt + 1);
						white_cnt++;
					} else {
						black_cnt = white_lvl - c;
						output[ptr_out++] = size_l - 1;
						if (max_len == DEF_MAX) {
							max_len = size_l * 5;
						}
						if (size_l > 8) {
							filter = 1;
						}
						block_ptr = size_l;
						line_mode = 0;
					}
				}
			} else {
				c = -1;
			}
			last_c = c;
		}
	}
	if (ii == DBG_SEG) {
		cout << endl;
	}
	return ptr_out;
}

static inline void swp(int &a, int &b) {
	int tmp = a;
	a = b;
	b = tmp;
}

static bool test_pattern(int a[3], int b[3], vector<center> pat_v[N_PATTERN]) {
	bool ok = true;
	if (a[0] != b[0]) {
		if (a[1] == b[0]) {
			swp(a[0], a[1]);
		} else if (a[0] == b[1]) {
			swp(b[0], b[1]);
		} else if (a[1] == b[1]) {
			swp(a[0], a[1]);
			swp(b[0], b[1]);
		} else {
			ok = false;
		}
	}
	if (ok) {
		//ok = a[1]!=b[1];
		double orient = (pat_v[0][a[1]].cen.x - pat_v[0][a[0]].cen.x)
				* (pat_v[0][b[1]].cen.y - pat_v[0][b[0]].cen.y)
				- (pat_v[0][a[1]].cen.y - pat_v[0][a[0]].cen.y)
						* (pat_v[0][b[1]].cen.x - pat_v[0][b[0]].cen.x);
		if (orient < 0) {
			swp(a[1], b[1]);
			swp(a[2], b[2]);
		}
	}
	return ok;
}

// funkce na nalezeni obrazcu, nalezeni polohy kalibracniho L, nalezeni polohy dron
bool find_pattern(Mat &orig, Mat &img, vector<center> &centers,
		vector<Point2f> &poi_img, vector<Point2f> &drone) {
	bool ret = false;
	int black_sum[4];
	vector<center> pat_v[N_PATTERN];
	Mat rot;
	vector<Point2f> projectedPoints;
	bool point_ok[16];
	double ellipse_size[16];
	// pro kazdou detekovanou oblast
	for (int i = (int) centers.size() - 1; i >= 0; i--) {
		double centx = centers[i].cen.x;
		double centy = centers[i].cen.y;
		int size_min, size_max;
		int valid[16];
		int p_pat[N_PATTERN];
		int fail = 0, fail2 = 0;
		int det;
		Point p;
		vector<Point> point_ellipse(16);
		if (DBG_SEG >= -1) {
			cout << "Segment " << i << " cent " << centx << "," << centy
					<< endl;
		}
		for (int pat = 0; pat < N_PATTERN; pat++) {
			p_pat[pat] = 0;
		}
		size_max = 0;
		size_min = 10000;
		for (int j = 0; j < 16; j++) {
			point_ok[j]=false;
			// detekce paprsku v 16-ti smerech
			det = detect_line(orig, centx, centy, j, centers[i].min_c, valid,
					black_sum, i);
			if (i == DBG_SEG) {
				cout << "Line " << j << " detected " << det;
				for (int z = 0; z < det; z++) {
					cout << ", " << valid[z];
				}
				cout << endl;
			}
			if (det >= 7) {
				// paprasek obsahuje dve cerne plochy ve vzdalenosti 5*polomer vnitrniho kruhu
				int s1 = 1 + valid[2] - valid[1], s2 = 1 + valid[4] - valid[3],
						s3 = 1 + valid[6] - valid[5];
				int v1 = (valid[3] - valid[0]) - 1, v2 = (valid[7] - valid[4]
						- 1);
				if (valid[6] * usec_size[j] > size_max) {
					size_max = valid[6] * usec_size[j];
				}
				if (valid[6] * usec_size[j] < size_min) {
					size_min = valid[6] * usec_size[j];
				}
				if (valid[6] < 18) {
					// prumer obrazce je mensi nez 36 pixelu - detekce pres plochu tmavych casti
					int pat;
					int limit = 1;
					if (black_sum[0] < 0.7 * black_sum[1] && v2 > limit) {
						// vzor 0  tenka, tlusta  cara
						pat = 0;
						p_pat[pat]++;
					} else if (black_sum[0] * 0.7 > black_sum[1]
							&& v1 > limit) {
						// vzor 1 tlusta tenka cara
						pat = 1;
						p_pat[pat]++;
					} else {
						if (valid[6] >= 6) {
							if (black_sum[0] > lim_tlu[valid[6] - 6]
									&& black_sum[1] > lim_tlu[valid[6] - 6]
									&& v1 > limit && v2 > limit) {
								//  vzor 3 tlusta, tlusta cara
								p_pat[3]++;
								pat = 3;
							} else if ((black_sum[0] < lim_ten[valid[6] - 6]
									&& black_sum[1] < lim_ten[valid[6] - 6])
									|| (v1 <= limit && v2 <= limit)) {
								// vzor 2 tenka, tenka cara
								p_pat[2]++;
								pat = 2;
							} else {  // muze to byt cokoliv
								p_pat[0]++;
								p_pat[1]++;
								p_pat[2]++;
								p_pat[3]++;
							}
						} else { // muze to byt cokoliv
							pat = -1;
							p_pat[0]++;
							p_pat[1]++;
							p_pat[2]++;
							p_pat[3]++;
						}
					}
					if (i == DBG_SEG) {
						cout << pat << " Pat black sum " << black_sum[0] << ","
								<< black_sum[1] << " s " << s1 << ","
								<< (s2 * pattern[pat][0]) << "; ";
						cout << s3 << "," << (s2 * pattern[pat][1]) << " lvl "
								<< valid[6] << " v1 " << v1 << " v2 " << v2
								<< endl;
					}
				} else {
					// obrazec vetsi nez 36 pixelu - pomery bileho mezikruzi a tmavych casti
					for (int pat = 0; pat < N_PATTERN; pat++) {
						if (aprox(s1, s2 * pattern[pat][0], valid[6])
								&& aprox(s3, s2 * pattern[pat][1], valid[6])) {
							if (i == DBG_SEG) {
								cout << pat << " Pat " << s1 << ","
										<< (s2 * pattern[pat][0]) << "; ";
								cout << s3 << "," << (s2 * pattern[pat][1])
										<< endl;
							}
							p_pat[pat]++;
						}
					}
				}
				if (det>7) {
					point_ok[j] = true;
					point_ellipse[j].x = centers[i].cen.x
						+ usec_x[j] * (valid[6] + valid[7]) / 2.0;
					point_ellipse[j].y = centers[i].cen.y
						+ usec_y[j] * (valid[6] + valid[7]) / 2.0;
					ellipse_size[j] = usec_size[j] * (valid[6] + valid[7] + 2)
						/ 2.0;
				}
			} else {
				// nedetekovany dve tmave casti
				fail++;
				if (det < 6) { // nedetekovana ani jedna tmava cast
					fail2++;
				}
			}
		}
		if (DBG_SEG >= -1) {
			cout << "Fail " << fail << " Pattern 0 - " << p_pat[0] << " 1 - "
					<< p_pat[1] << " 2 - " << p_pat[2] << " 3 - " << p_pat[3]
					<< endl;
		}

		// vyhodnoceni cele oblasti
		if (fail <= 4 && fail2 < 2) {
			// alespon 12 car detekovalo dve cerne oblasti a alespon 14 car detekovalo alespon jednu oblast
			bool repeat = true;
			double max_size, min_size;
			int fail_sz = 0;
			while (repeat) {
				repeat = false;
				max_size = 0;
				min_size = 10000;
				for (int j = 0; j < 16; j++) {
					if (!point_ok[j]) {
						if (DBG_SEG > -1) {
							cout << "Dopocitavani bodu "<< j<<endl;
						}
						if (point_ok[(j + 1) % 16] && point_ok[(j + 15) % 16]) {
							// doplnovani bodu na elipse podle prumeru sousednich bodu
							ellipse_size[j] = (ellipse_size[(j + 1) % 16]+ellipse_size[(j + 15) % 16])/2.0;
							point_ellipse[j].x = centers[i].cen.x
									+ usec_x[j] * ellipse_size[j]
											/ usec_size[j];
							point_ellipse[j].y = centers[i].cen.y
									+ usec_y[j] * ellipse_size[j]
											/ usec_size[j];
							point_ok[j] = true;
						} else if (point_ok[(j + 8) % 16]) {
							// doplnovani bodu na elipse podle protejsiho bodu
							ellipse_size[j] = ellipse_size[(j + 8) % 16];
							point_ellipse[j].x = centers[i].cen.x
									+ usec_x[j] * ellipse_size[j]
											/ usec_size[j];
							point_ellipse[j].y = centers[i].cen.y
									+ usec_y[j] * ellipse_size[j]
											/ usec_size[j];
							point_ok[j] = true;
						} else if (point_ok[(j + 1) % 16]) {
							// doplnovani bodu na elipse podle jednoho sousedniho bodu
							ellipse_size[j] = ellipse_size[(j + 1) % 16];
							point_ellipse[j].x = centers[i].cen.x
									+ usec_x[j] * ellipse_size[j]
											/ usec_size[j];
							point_ellipse[j].y = centers[i].cen.y
									+ usec_y[j] * ellipse_size[j]
											/ usec_size[j];
							point_ok[j] = true;
						} else if (point_ok[(j + 15) % 16]) {
							// doplnovani bodu na elipse podle jednoho sousedniho bodu
							ellipse_size[j] = ellipse_size[(j + 15) % 16];
							point_ellipse[j].x = centers[i].cen.x
									+ usec_x[j] * ellipse_size[j]
											/ usec_size[j];
							point_ellipse[j].y = centers[i].cen.y
									+ usec_y[j] * ellipse_size[j]
											/ usec_size[j];
							point_ok[j] = true;
						} else {
							repeat = true;
						}
					}
					if (point_ok[j]) {
						// zjistovani maximalniho a minimalniho prumeru elipsy
						if (max_size < ellipse_size[j]) {
							max_size = ellipse_size[j];
						}
						if (min_size > ellipse_size[j]) {
							min_size = ellipse_size[j];
						}
					}
				}
			}
			RotatedRect ell = fitEllipse(point_ellipse);
			int max = 0;
			int max_p = -1;
			double cos_a = 1, sin_a = 0, koef = 1;
			if (ell.size.width < 3 * max_size
					&& ell.size.height < 3 * max_size) {
				if (DBG_SEG == i) {
				   cout << "Ellipse center " << ell.center << " size " << ell.size << endl;
				}
				sin_a = sin(ell.angle * M_PI / 180);
				cos_a = cos(ell.angle * M_PI / 180);
				koef = ell.size.width / ell.size.height;
				Mat rot =
						(Mat_<double>(2, 2) << cos(ell.angle), sin(ell.angle), -sin(
								ell.angle), cos(ell.angle));
				for (int j = 0; j < (int) point_ellipse.size(); j++) {
					if (DBG_SEG == i) {
						/*dr_point(img, point_ellipse[j], 4);*/
						cout << "Ell point "<<j<< " je " << point_ellipse[j] << " size " << ellipse_size[j] << endl;
					}
					Point2f vec;
					vec.x = point_ellipse[j].x - ell.center.x;
					vec.y = point_ellipse[j].y - ell.center.y;
					Point2f rot_p;
					rot_p.x = vec.x * cos_a + vec.y * sin_a;
					rot_p.y = (-vec.x * sin_a + vec.y * cos_a) * koef;
					double siz = 2.0
							* sqrt(rot_p.x * rot_p.x + rot_p.y * rot_p.y)
							- ell.size.width;
					if (siz < -3 || siz > 3) {
						fail_sz++;
						if (DBG_SEG == i) {
							cout << " siz " << siz << " ell " << ell.size
									<< " fail " << fail_sz << endl;
						}
						double kk = ell.size.width
								/ (2.0
										* sqrt(
												rot_p.x * rot_p.x
														+ rot_p.y * rot_p.y));
						rot_p.x *= kk;
						rot_p.y *= kk / koef;
						vec.x = rot_p.x * cos_a - rot_p.y * sin_a;
						vec.y = rot_p.x * sin_a + rot_p.y * cos_a;
						if (DBG_SEG == i) {
							/*dr_point(img, vec + ell.center, 5);*/
						}
					}
				}
			}
			if (DBG_SEG>=0) {
			cout << "Fail " << fail << " fail ellipse " << fail_sz
					<< " Pattern 0 - " << p_pat[0] << " 1 - " << p_pat[1]
					<< " 2 - " << p_pat[2] << " 3 - " << p_pat[3] << endl;
			}
			// detekce druhu obrazce
			for (int pat = 0; pat < N_PATTERN; pat++) {
				if (p_pat[pat] > max) {
					max = p_pat[pat];
					max_p = pat;
				}
				if (p_pat[pat] >= 14 && fail_sz < 2) {
					// spolehlive detekovany obrazec
					if (DBG_SEG >= -1) {
					cout << "Good detected pattern " << pat << "," << p_pat[pat]
							<< " Segment " << i << " cent " << centx << ","
							<< centy << " size " << size_min << "," << size_max
							<< " plocha " << centers[i].size << endl;
					}
					pat_v[pat].push_back(centers[i]);
					/*dr_point(img, centers[i].cen, pat);*/
				}
			}
			if (max < 14 && max >= 8 && fail_sz < 4) {
				// mene spolehlive detekovany obrazec
				//cout << "Segement "<< i << " max " << max << endl;
				if (DBG_SEG >= -1) {
				cout << "Weak Detected pattern " << max_p << "," << p_pat[max_p]
						<< " Segment " << i << " cent " << centx << "," << centy
						<< " size " << size_min << "," << size_max << " plocha "
						<< centers[i].size << endl;
				}
				pat_v[max_p].push_back(centers[i]);
				/*dr_point(img, centers[i].cen, max_p);*/
			} else if (max < 10) {
				/*dr_point(img, centers[i].cen, 6);*/
			}
		} else {
			/*dr_point(img, centers[i].cen, 6);*/
		}
	}

	// detekce L kalibracniho obrazce
	int line_ptr = 0;
	int line_ind[3][3];
	bool found = false;
	if (pat_v[0].size() >= 3 && pat_v[1].size() >= 2) {
		for (unsigned int m = 0; m < pat_v[0].size() && line_ptr < 3; m++) {
			for (unsigned int o = 0; o < pat_v[1].size() && line_ptr < 3; o++) {
				double exp_x = 2 * pat_v[1][o].cen.x - pat_v[0][m].cen.x;
				double exp_y = 2 * pat_v[1][o].cen.y - pat_v[0][m].cen.y;
				double dist = sqrt(
						sqr(pat_v[1][o].cen.x - pat_v[0][m].cen.x)
								+ sqr(pat_v[1][o].cen.y - pat_v[0][m].cen.y))
						/ 4.0;
				for (unsigned int n = m + 1; n < pat_v[0].size(); n++) {
					double diff_x = pat_v[0][n].cen.x - exp_x;
					double diff_y = pat_v[0][n].cen.y - exp_y;
					if (DBG_SEG >= -1) {
						cout << exp_x << "," << exp_y << " realna "
								<< pat_v[0][n].cen << endl;
						cout << diff_x << "," << diff_y << " dist " << dist
								<< " m " << m << " o " << o << " n " << n
								<< endl;
					}
					if (diff_x < dist && diff_x > -dist && diff_y < dist
							&& diff_y > -dist) {
						line_ind[line_ptr][0] = m;
						line_ind[line_ptr][1] = n;
						line_ind[line_ptr++][2] = o;
						break;
					}
				}
			}
		}
		if (DBG_SEG >= -1) {
			cout << line_ptr << endl;
			cout << line_ind[0][0] << "," << line_ind[0][1] << ","
					<< line_ind[0][2] << " - 0" << endl;
			cout << line_ind[1][0] << "," << line_ind[1][1] << ","
					<< line_ind[1][2] << " - 1" << endl;
			cout << line_ind[2][0] << "," << line_ind[2][1] << ","
					<< line_ind[2][2] << " - 2" << endl;
		}
		if (line_ptr >= 2) {
			if (test_pattern(line_ind[0], line_ind[1], pat_v)) {
				found = true;
			} else if (line_ptr > 2) {
				if (test_pattern(line_ind[0], line_ind[2], pat_v)) {
					memcpy(line_ind[1], line_ind[2], 3 * sizeof(int));
					found = true;
				} else if (test_pattern(line_ind[1], line_ind[2], pat_v)) {
					memcpy(line_ind[0], line_ind[1], 3 * sizeof(int));
					memcpy(line_ind[1], line_ind[2], 3 * sizeof(int));
					found = true;
				}
			}
		}
		if (found) {
			line(img, pat_v[0][line_ind[0][0]].cen,
					pat_v[0][line_ind[0][1]].cen, CV_RGB(255, 255, 255), 2);
			//line(img, pat_v[0][line_ind[0][0]].cen,
			//		pat_v[1][line_ind[0][2]].cen, CV_RGB(255, 0, 0)); // blue y axis - on the pattern
			line(img, pat_v[0][line_ind[1][0]].cen,
					pat_v[0][line_ind[1][1]].cen, CV_RGB(255, 255, 255), 2);
			//line(img, pat_v[0][line_ind[1][0]].cen,
			//		pat_v[1][line_ind[1][2]].cen, CV_RGB(0, 255, 0)); // green x axis - on the pattern
			poi_img[0] = pat_v[0][line_ind[0][0]].cen;
			poi_img[1] = pat_v[1][line_ind[0][2]].cen;
			poi_img[2] = pat_v[0][line_ind[0][1]].cen;
			poi_img[3] = pat_v[1][line_ind[1][2]].cen;
			poi_img[4] = pat_v[0][line_ind[1][1]].cen;
			ret = true;
			if (pat_v[0].size() == 4) {
				for (int kk = 0; kk < 4; kk++) {
					if (kk != line_ind[0][0] && kk != line_ind[0][1]
							&& kk != line_ind[1][1]) {
						drone[0] = pat_v[0][kk].cen;
					}
				}
			} else {
				drone[0].x = -1;
			}
			if (pat_v[1].size() == 3) {
				for (int kk = 0; kk < 3; kk++) {
					if (kk != line_ind[0][2] && kk != line_ind[1][2]) {
						drone[1] = pat_v[1][kk].cen;
					}
				}
			} else {
				drone[1].x = -1;
			}
		}
	}
	if (!ret) {
		/*if (f_out!=NULL) {
		 fprintf(f_out, "-1000000,-1000000,-1000000\n");
		 }*/
		//imshow("response", *img);
		//waitKey(0);
	}
	int start = 2;
	if (!found) {
		start = 0;
	}
	for (int jj = start; jj < 4; jj++) {
		if (pat_v[jj].size() == 1) {
			drone[jj] = pat_v[jj][0].cen;
		} else {
			drone[jj].x = -1;
		}
	}
	return ret;
}


#define N_PATTERN 4
// calibration matrix
//double web_cam_data[3][3]={
//	{651.675, 0.0, 333.2}, {0.0, 652.7, 248.16}, {0.0, 0.0, 1.0}}; // puvodni
//double web_cam_data[3][3]={
//		{528.368587146877990, 0.0, 324.926464550010850},
//		{0.0, 528.688700687079180, 245.366881255903880},
//		{0.0, 0.0, 1.0}}; // note2
//double web_cam_data[3][3]={
//		{574.140327388537, 0.0, 311.081667760924},
//		{0.0, 575.014813496568, 234.723439747463},
//		{0.0, 0.0, 1.0}}; // note3 640x480
double web_cam_data[3][3]={
		{583.363381694481860, 0.0, 311.509865407385970},
		{0.0, 584.132102401364930, 166.409785807330420},
		{0.0, 0.0, 1.0}}; // note3 640x360
Mat web_cam_mat(3,3, CV_64F, web_cam_data);

// distortion
//double web_dist_data[5]={-1.3349093781525635e-02, -2.0050669779655408e-01,
//	       -3.9676659563869380e-03, 1.1082368963856762e-02,
//	       6.7825810413216636e-01}; // puvodni
//double web_dist_data[5]={0.055047671049250 , -0.164401720874435 ,
//		-0.000113362700144 , -0.000680641687715 , 0.000000000000000 }; // note2
//double web_dist_data[5]={0.134890508717150, -0.335748193184664,
//		0.003211392397653, -0.001077363162804, 0.000000000000000}; // note3 640x480
double web_dist_data[5]={ 0.128907191360330, -0.327420875668058,
		-0.001622179753070, -0.001039557637961, 0.000000000000000}; // note3 640x360


Mat web_dist_coef(5,1,CV_64F, web_dist_data);

Point3f arr[] = { { 0, 0, 0 }, { 0, 60, 0 }, { 0, 120, 0 }, { 60, 0, 0 },
		{ 120, 0, 0 } };
vector<Point3f> poi_pattern(arr, arr + sizeof(arr)/sizeof(arr[0]));
Point3f cube[] = { { 0, 0, 0 }, { 0, 50, 0 }, { 50, 0, 0 }, { 50, 50, 0 },
		{ 0, 0, 50 }, { 0, 50, 50 }, { 50, 0, 50 }, { 50, 50, 50 },
		{0, 5, 0}, {50, 5, 0}, {0, 2, 0}, {50, 2, 0},
		{5, 0, 0}, {5, 50, 0}, {2, 0, 0}, {2, 50, 0},
		{5, 5, 0}, {5, 5, 50}, {2, 2, 0}, {2, 2, 50}};
vector<Point3f> poi_cube(cube, cube + sizeof(cube)/sizeof(cube[0]));

Point2f frame_arr[] = { { 0, 0}, { 639, 0}, { 639, 359}, { 0, 359} };
vector<Point2f> img_frame(frame_arr, frame_arr + sizeof(frame_arr)/sizeof(frame_arr[0]));



struct buffer {
	/**
	 * Buffer for storing of orientation data from vision, and for extrapolation in case of vision failure
	 * using the stored data and data from inner sensors.
	 */
  int max_buffer_size = 5; // size of the buffer for time and tvec
  int min_working_size = 2;
  bool ready = false; // if it is able to extrapolate
  Mat tvec_buff[5];
  timestamp_t time_buff[5];
  Mat tvec_diff[5]; // in practise should be N - 1
  timestamp_t time_diff[5]; // in practise should be N - 1
  Mat quat_diff; // quaternion difference between rotation of projectPoints based on OCV vision and IMU sensor
  double weights[5]; // in practise should be N - 1, but due to walk through circular buffer, one field is always skipped
  double weight_sum;

  Mat rvec_buff[2]; // deprecated, no need for this
  int index = -1; // init index for circular buffer
  int counter = 0; // total number of added buffer entries

  // new data into buffer are added
  // tvec rvec  - pose from vision
  // t_stamp    - time stamp of the event
  // quat_sense - orientation from sensors
  void add(Mat tvec, Mat rvec, timestamp_t t_stamp, Mat quat_sens) {
	  //LOGD("find rot trans : ***add1 tvec %f %f %f", tvec.at<double>(0), tvec.at<double>(1), tvec.at<double>(2));
	  Mat quat_rvec, quat_sens_local;
	  counter++;
	  if (counter >= min_working_size) {
		  ready = true;
	  }

	  // deprecated - only for testing
	  rvec_buff[1] = rvec_buff[0];
	  rvec_buff[0] = rvec.clone();

	  rvec2quat(rvec, quat_rvec);
	  quat_sens_local = quat_sens.clone();

	  ///////// ver. 1
//	  // equations:  quat_rvec = quat_diff * quat_sens ...
//	  //			 quat_diff = quat_rvec * quat_sens^-1
//	  // as rotation for projectPoints and for camera is inverted, inversion of sensor data is needed
//	  // therefore no inversion is carried out at this point
//	  quat_diff = quatmulti(quat_rvec, quat_sens);

	  ///////// ver. 2
	  // \\\***/// move rvec to model system
	  quatinv(quat_rvec);
	  quatinv(quat_sens_local); // needed for equation ... quat_diff will be in model system
	  // equations:  quat_rvec = quat_diff * quat_sens ...
	  //			 quat_diff = quat_rvec * quat_sens^-1
	  quat_diff = quatmulti(quat_rvec, quat_sens_local);

	  ///////// ver. 2
	  // \\\***/// move tvec to model system
	  Mat rot, tvec2;
	  Rodrigues(rvec, rot);
	  rot = rot.t(); // rotaci musime prevratit (matrix i vector)
	  tvec2 = -rot*tvec;

	  // Shuffling edition
	  //timestamp_t t0 = get_timestamp();
	  tvec_buff[4] = tvec_buff[3];
	  tvec_buff[3] = tvec_buff[2];
	  tvec_buff[2] = tvec_buff[1];
	  tvec_buff[1] = tvec_buff[0];
	  tvec_buff[0] = tvec2.clone();

	  time_buff[4] = time_buff[3];
	  time_buff[3] = time_buff[2];
	  time_buff[2] = time_buff[1];
	  time_buff[1] = time_buff[0];
	  time_buff[0] = t_stamp;
	  //timestamp_t t1 = get_timestamp();
	  //double mils = (t1 - t0) / 1000.0L;
	  //LOGD("find rot trans : ARRAY SHUFFLE TIMR %f mils",mils);

	  //LOGD("find rot trans : ***add2 tvec %f %f %f, indx %d", tvec_buff[0].at<double>(0), tvec_buff[0].at<double>(1), tvec_buff[0].at<double>(2), index);
  };

  // for translation part of pose extrapolation
  // t_stamp - time stamp for extrapolation duration
  // rvec    - needed for proper coordinate system transformation
  Mat extrapolate_tvec(timestamp_t t_stamp, Mat rvec)  {
	  //LOGD("find rot trans : ***extr1 tvec %f %f %f", tvec_buff[0].at<double>(0), tvec_buff[0].at<double>(1), tvec_buff[0].at<double>(2));
	  Mat tvec;
	  //timestamp_t t0 = get_timestamp();

	  // Shuffling edition
	  tvec.create(3, 1, CV_64F);
	  	  if (ready) {
	  		  timestamp_t t_0 = t_stamp - time_buff[0];
	  		  weight_sum = 0;
	  		  int max = counter < max_buffer_size ? counter : max_buffer_size;
	  		  //LOGD("find rot trans : max %d", max);
	  		  for (int c = 0; c < max - 1; ++c) {
	  			  time_diff[c] = time_buff[c] - time_buff[c+1];
	  			  weights[c] = 1/double(t_stamp - time_buff[c]);
	  			  weight_sum += weights[c];
	  		      tvec_diff[c] = tvec_buff[c] - tvec_buff[c+1];
	  		      //LOGD("find rot trans : time_diff[c] %llu, weights[c] %f, weight_sum %f ", time_diff[c], weights[c], weight_sum);
	  		      //LOGD("find rot trans : tvec_diff[c] %f %f %f", tvec_diff[c].at<double>(0), tvec_diff[c].at<double>(1), tvec_diff[c].at<double>(2));
	  		  }
	  		  tvec = tvec_buff[0].clone();
	  		  for (int c = 0; c < max - 1; ++c) {
	  			  weights[c] /= weight_sum;
	  			  tvec +=  tvec_diff[c] * ((t_0/double(time_diff[c])) * weights[c]);
	  			  //LOGD("find rot trans : weights[c] %f ", weights[c]);
	  			  //LOGD("find rot trans : tvec %f %f %f", tvec.at<double>(0), tvec.at<double>(1), tvec.at<double>(2));
	  		  }

		      ///////// ver. 2
		      /// \\\***/// return tvec from model system
		      Mat rot;
		      Rodrigues(rvec, rot);
		      tvec = -rot*tvec;
	  	  }
	  //timestamp_t t1 = get_timestamp();
	  //double mils = (t1 - t0) / 1000.0L;
	  //LOGD("find rot trans : EXTRAPOLATE TIMR %f mils",mils);
	  return tvec;
  }

  // for rotation part of pose extrapolation
  // quat_sens - orientation from sensors
  Mat extrapolate_rvec(Mat quat_sens) {
	  Mat quat_rvec, rvec;
	  quat_rvec.create(3, 1, CV_64F);

	  ///////// ver. 1
//	  // equation: quat_rvec = quat_diff * quat_sens;
//	  // as rotation for projectPoints and for camera is inverted, inversion of sensor data is needed
//	  quatinv(quat_sens);
//	  quat_rvec = quatmulti(quat_diff, quat_sens);

	  ///////// ver. 2
	  // equation: quat_rvec = quat_diff * quat_sens;
	  // \\\***/// return rvec from model system
	  //quatinv(quat_sens); // no inversion needed, model system is used
	  quat_rvec = quatmulti(quat_diff, quat_sens);
	  quatinv(quat_rvec); // back to camera system

	  // transformation to OCV rotation vector 'rvec'
	  quat2rvec(quat_rvec, rvec);

	  // rvec = rvec_buff[0].clone(); // FIXED rotation for extrapolation - testing
	  return rvec;
  }

};


static int last=0;
static vector<Point2f> projectedPoints(20); // previous drawing continues even if the pattern is lost
static vector<Point2f> projectedPoints2(20);
static buffer myBuffer;
static const Scalar colorFound = CV_RGB(0, 255, 255);
static const Scalar colorExtrapolate = CV_RGB(0, 255, 255); // CV_RGB(255, 255, 0);
static int thickLine = 2, thinLine = 1;
static int framCount = 0;

// main function for pose estimation
/* mGr 		- input - gray image
 * mRgb 	- input/ouput - colour image
 * mRot 	- output - rotation matrix
 * mTrans 	- ouput - translation vector
 * mRvec	- ouput - rotation (Euler) vector
 * mQuat	- output - unite rotation quaternion
 * mSensQuat- input - orientation data from sensors
 * mTrans2  - output - secondary translation vector - extrapolated even if vison is working
 * mQuat2	- output - secondary unite rotation vector - extrapolated even if vison is working
 * */
bool find_rot_trans(Mat &mGr, Mat &mRgb, Mat &mRot, Mat &mTrans, Mat &mRvec, Mat &mQuat, Mat &mSensQuat, Mat &mTrans2, Mat &mQuat2) {
	vector<center> centers;
	vector<Point2f> poi_img(5), drone(4);
	Mat buf, rvec, tvec, rot, quat_cam, rvec2, tvec2, rot2, quat_cam2;
	// TODO better to propagate from java method - onCameraFrame
	// ... doesn't matter, as only time difference between positions is important, not the precise time
	timestamp_t t_stamp = get_timestamp();
	LOGD("c++ frametime %llu", t_stamp);
	bool found;
	Scalar color;
	bool measureMode = true; // allows for exporting both vision and extrapolated pose at the same time, and draw 2 cubes
							 // extrapolated data are computed even if pattern is found
	bool fusionAllowed = true;

	centers.clear();
	find_centers(mGr, centers, buf);

	found = false;
	found = find_pattern(mGr, mRgb, centers, poi_img, drone);

	/*found = true;*/

	// compute translation and rotation from buffered pose and new sensor orientation
	if (fusionAllowed && (!found || measureMode) && myBuffer.ready) {
		LOGD("find rot trans: extrapolate ...");

		//rvec = myBuffer.rvec_buff[0]; // deprecated - only previously buffered rotation
		rvec = myBuffer.extrapolate_rvec(mSensQuat);

		tvec = myBuffer.extrapolate_tvec(t_stamp, rvec);

		/*// let's play a game 0 ... modify and then restor tvec
		Mat rot, tvec2;
		Rodrigues(rvec, rot);
		rot = rot.t(); // rotaci musime prevratit (matrix i vector)
		tvec2 = -rot*tvec;
		rot = rot.t(); // vratim zpet na netransponovanou
		tvec = -rot*tvec2;*/

		LOGD("find rot trans extr: tvec %f %f %f,... rvec %f %f %f ",
							tvec.at<double>(0), tvec.at<double>(1), tvec.at<double>(2),
							rvec.at<double>(0), rvec.at<double>(1), rvec.at<double>(2));
		color = colorExtrapolate;

		if (measureMode) {
			tvec2 = tvec.clone();
			rvec2 = rvec.clone();
		}
	}

	// compute translation and rotation from vision
	if (found) {
		// poi_pattern - set above :: objectPoints � Array of object points in the object coordinate
		// poi_img - found by 'find_pattern' function above :: imagePoints � Array of corresponding image points
		// web_cam_mat + web_dist_coef ... camera calibration
		if (solvePnP(poi_pattern, poi_img, web_cam_mat, web_dist_coef, rvec, tvec, false, CV_ITERATIVE)) {

			myBuffer.add(tvec, rvec, t_stamp, mSensQuat); // add to buffer when pattern found and pose solved
			// quat_rvec is transposed in comparison with exported quat_cam for matlab

			LOGD("find rot trans : tvec %f %f %f,... rvec %f %f %f ",
					tvec.at<double>(0), tvec.at<double>(1), tvec.at<double>(2),
					rvec.at<double>(0), rvec.at<double>(1), rvec.at<double>(2));

			color = colorFound;
		} else {
			LOGD("find rot trans: found 1 solvePnP 0"); // never ever happend, never, just forget about it
		}
	}

	// draw virtual object based on computed pose - method independent
	if (found || (myBuffer.ready && fusionAllowed)) {

		// let's play a game 1 ... use all quat function to modify and then restor rvec
		/*Mat tmp_rvec_quat, tmp_multi;
		Mat tmp_rvec = rvec.clone();
		rvec2quat(tmp_rvec, tmp_rvec_quat);
		tmp_rvec_quat.at<double>(0) = tmp_rvec_quat.at<double>(0) * 3;
				tmp_rvec_quat.at<double>(1) = tmp_rvec_quat.at<double>(1) * 3;
				tmp_rvec_quat.at<double>(2) = tmp_rvec_quat.at<double>(2) * 3;
				tmp_rvec_quat.at<double>(3) = tmp_rvec_quat.at<double>(3) * 3;
		quatnorm(tmp_rvec_quat);
		Mat tmp_screw = mSensQuat.clone();
		tmp_multi = quatmulti(tmp_screw, tmp_rvec_quat);
		quatinv(tmp_screw);
		tmp_rvec_quat = quatmulti(tmp_screw, tmp_multi);
		quat2rvec(tmp_rvec_quat, tmp_rvec);
		rvec = tmp_rvec.clone();*/


		// let's play a game 2 ... use ses data + opt. c2s rotation
		/*Mat quat_c2s;
		quat_c2s.create(4, 1, CV_64F);
		quat_c2s.at<double>(0) = 0;
		quat_c2s.at<double>(1) = 0.7071;
		quat_c2s.at<double>(2) = -0.7071;
		quat_c2s.at<double>(3) = 0;
		//quatinv(mSensQuat);
		//mSensQuat = quatmulti(quat_c2s, mSensQuat);
		quat2rvec(mSensQuat, rvec);*/


		// let's play a game 3 ... identity
		/*Mat quat_eye;
		quat_eye.create(4, 1, CV_64F);
		quat_eye.at<double>(0) = 0;
		quat_eye.at<double>(1) = 0.7071;
		quat_eye.at<double>(2) = -0.7071;
		quat_eye.at<double>(3) = 0;
		quat2rvec(quat_eye, rvec);*/

		// FIXED tvec
		/*tvec.at<double>(0) = 20;
		tvec.at<double>(1) = 70;
		tvec.at<double>(2) = 400;*/

		// final offset rotation of rvec
		/*Mat quat_c2s, tmp_quat_rvec, rvec2;
		  quat_c2s.create(4, 1, CV_64F);
		  quat_c2s.at<double>(0) = 0;
		  quat_c2s.at<double>(1) = 0.7071;
		  quat_c2s.at<double>(2) = -0.7071;
		  quat_c2s.at<double>(3) = 0;
		  rvec2 = rvec.clone();
		  rvec2quat(rvec2, tmp_quat_rvec);
		  tmp_quat_rvec = quatmulti(quat_c2s, tmp_quat_rvec);
		  quat2rvec(tmp_quat_rvec, rvec2);
		  rvec = rvec2.clone();*/


		projectPoints(poi_cube, rvec, tvec, web_cam_mat, web_dist_coef, projectedPoints);
		// modifications for output
		Rodrigues(rvec, rot);
		rot = rot.t(); // rotaci musime prevratit (matrix i vector)
		rvec = -rvec; // rotaci musime prevratit (matrix i vector)
		tvec = -rot*tvec;

		rvec2quat(rvec, quat_cam);
		mRot = rot; // returning rotation and translation of camera - modified for matlab plot
		mTrans = tvec;
		mRvec = rvec;
		mQuat = quat_cam;

		// drawing the cube
		//line(mRgb, projectedPoints[0],projectedPoints[1], color, thinLine);
		//line(mRgb, projectedPoints[0],projectedPoints[2], color, thinLine);
		line(mRgb, projectedPoints[0],projectedPoints[2], CV_RGB(0, 255, 0), thickLine); // x green
		line(mRgb, projectedPoints[0],projectedPoints[1], CV_RGB(255, 0, 0), thickLine); // y blue
		line(mRgb, projectedPoints[2],projectedPoints[3], color, thinLine);
		line(mRgb, projectedPoints[1],projectedPoints[3], color, thinLine);

		line(mRgb, projectedPoints[1],projectedPoints[5], color, thinLine);
		line(mRgb, projectedPoints[2],projectedPoints[6], color, thinLine);
		line(mRgb, projectedPoints[3],projectedPoints[7], color, thinLine);
		//line(mRgb, projectedPoints[0],projectedPoints[4], color, thinLine);
		line(mRgb, projectedPoints[0],projectedPoints[4], CV_RGB(0, 0, 255), thickLine); // z red

		line(mRgb, projectedPoints[4],projectedPoints[5], color, thinLine);
		line(mRgb, projectedPoints[4],projectedPoints[6], color, thinLine);
		line(mRgb, projectedPoints[6],projectedPoints[7], color, thinLine);
		line(mRgb, projectedPoints[5],projectedPoints[7], color, thinLine);


//		if (!found) { // frame around the image
//			line(mRgb, img_frame[0], img_frame[1], CV_RGB(255, 255, 0));
//			line(mRgb, img_frame[1], img_frame[2], CV_RGB(255, 255, 0));
//			line(mRgb, img_frame[2], img_frame[3], CV_RGB(255, 255, 0));
//			line(mRgb, img_frame[3], img_frame[4], CV_RGB(255, 255, 0));
//		}

	} else {
		mRot.setTo(0);
		mTrans.setTo(0);
		mRvec.setTo(0);
		mQuat.setTo(0);
	}

	// when pattern is found and measureMode is true, exttapolated data are also stored and second object is drawn
	if (fusionAllowed && measureMode && found && (myBuffer.counter > myBuffer.min_working_size)) {
		// myBuffer.ready cannot be used as one step more is needed for proper extrapolation after last addition to buffer

		/*LOGD("find rot trans measure: tvec2 %f %f %f,... rvec2 %f %f %f ",
							tvec2.at<double>(0), tvec2.at<double>(1), tvec2.at<double>(2),
							rvec2.at<double>(0), rvec2.at<double>(1), rvec2.at<double>(2));*/
		projectPoints(poi_cube, rvec2, tvec2, web_cam_mat, web_dist_coef, projectedPoints2);

		Rodrigues(rvec2, rot2);
		rot2 = rot2.t(); // rotaci musime prevratit (matrix i vector)
		rvec2 = -rvec2; // rotaci musime prevratit (matrix i vector)
		tvec2 = -rot2*tvec2;

		rvec2quat(rvec2, quat_cam2);
		mTrans2 = tvec2;
		mQuat2 = quat_cam2;

		// drawing the cube
		//line(mRgb, projectedPoints[0],projectedPoints[1], colorExtrapolate, thinLine);
		//line(mRgb, projectedPoints[0],projectedPoints[2], colorExtrapolate, thinLine);
		line(mRgb, projectedPoints[0],projectedPoints[2], CV_RGB(0, 255, 0), thickLine); // x green
		line(mRgb, projectedPoints[0],projectedPoints[1], CV_RGB(255, 0, 0), thickLine); // y blue
		line(mRgb, projectedPoints[2],projectedPoints[3], colorExtrapolate, thinLine);
		line(mRgb, projectedPoints[1],projectedPoints[3], colorExtrapolate, thinLine);

		line(mRgb, projectedPoints[1],projectedPoints[5], colorExtrapolate, thinLine);
		line(mRgb, projectedPoints[2],projectedPoints[6], colorExtrapolate, thinLine);
		line(mRgb, projectedPoints[3],projectedPoints[7], colorExtrapolate, thinLine);
		//line(mRgb, projectedPoints[0],projectedPoints[4], colorExtrapolate, thinLine);
		line(mRgb, projectedPoints[0],projectedPoints[4], CV_RGB(0, 0, 255), thickLine); // z red

		line(mRgb, projectedPoints[4],projectedPoints[5], colorExtrapolate, thinLine);
		line(mRgb, projectedPoints[4],projectedPoints[6], colorExtrapolate, thinLine);
		line(mRgb, projectedPoints[6],projectedPoints[7], colorExtrapolate, thinLine);
		line(mRgb, projectedPoints[5],projectedPoints[7], colorExtrapolate, thinLine);

	} else {
		if (measureMode && !found) {
			mTrans2.setTo(0);
			mQuat2.setTo(0);
		} else {
			mTrans2.setTo(-1);
			mQuat2.setTo(-1);
		}
	}
    return found;
}



extern "C" {
JNIEXPORT jboolean JNICALL Java_cz_ajio_simpleopencv1_HelloOpenCvActivity_FindFeatures(
		JNIEnv*, jobject, jlong addrGray, jlong addrRgba, jlong addrRot, jlong addrTrans,
		jlong addrRvec, jlong addrQuat, jlong addrSensQuat, jlong addrTrans2, jlong addrQuat2);

JNIEXPORT jboolean JNICALL Java_cz_ajio_simpleopencv1_HelloOpenCvActivity_FindFeatures(
		JNIEnv*, jobject, jlong addrGray, jlong addrRgba, jlong addrRot, jlong addrTrans,
		jlong addrRvec, jlong addrQuat, jlong addrSensQuat, jlong addrTrans2, jlong addrQuat2)
{
    Mat& mGr  = *(Mat*)addrGray;
    Mat& mRgb = *(Mat*)addrRgba;
    Mat& mRot  = *(Mat*)addrRot;
    Mat& mTrans  = *(Mat*)addrTrans;
    Mat& mRvec  = *(Mat*)addrRvec;
    Mat& mQuat  = *(Mat*)addrQuat;
    Mat& mSensQuat  = *(Mat*)addrSensQuat;
    Mat& mTrans2  = *(Mat*)addrTrans2;
	Mat& mQuat2  = *(Mat*)addrQuat2;
    vector<KeyPoint> v;

    // conversion of sensor data from float to double
	Mat mSensQuat64;
	mSensQuat64.create(4,1,CV_64F);
	mSensQuat.convertTo(mSensQuat64, CV_64F);

	// rotate sensor data from device sensor system to device camera system (without translation offset)
	Mat quat_c2s;
	quat_c2s.create(4, 1, CV_64F);
	quat_c2s.at<double>(0) = 0;
	quat_c2s.at<double>(1) = 0.7071;
	quat_c2s.at<double>(2) = -0.7071;
	quat_c2s.at<double>(3) = 0;
	// offset 'quat_c2s' needs to be first !!!
	mSensQuat64 = quatmulti(mSensQuat64, quat_c2s);

	LOGD("find rot trans : mSensQuat64 %f %f %f %f ",
			mSensQuat64.at<double>(0), mSensQuat64.at<double>(1), mSensQuat64.at<double>(2), mSensQuat64.at<double>(3));

    LOGD("find rot trans: FRAME NO %d", ++framCount);
    timestamp_t t0 = get_timestamp();
    // trans2 and quat2 is used for extrapolated data even when pattern is found, when it is not found, it is also used
    bool patternFind = find_rot_trans(mGr, mRgb, mRot, mTrans, mRvec, mQuat, mSensQuat64, mTrans2, mQuat2);
    timestamp_t t1 = get_timestamp();
    double mils = (t1 - t0) / 1000.0L;

    if (patternFind) {
    	LOGD("find rot trans : OK, %f mils",mils);
    } else {
    	LOGD("find rot trans: XX, %f mils",mils);
    }
    LOGD("*************************************",mils);

    return patternFind;

}


}






